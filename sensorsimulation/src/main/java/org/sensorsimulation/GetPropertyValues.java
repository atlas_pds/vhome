package org.sensorsimulation;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class GetPropertyValues {
	public HashMap<String,String> getPropValues() throws IOException {
		String result = "";
		 HashMap<String,String> map = new HashMap<String, String>();
		InputStream inputStream = null;
		try {
			Properties prop = new Properties();
			String propFileName = "C:\\Users\\PetrovAndrei\\git\\vhome\\sensorsimulation\\resources\\sensormessage.properties";
 
			inputStream = new FileInputStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} 
			//else {
			//	throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			//}
 
			//Date time = new Date(System.currentTimeMillis());
 
			// get the property value and print it out
			//String first = prop.getProperty("1.iter.1.duration");
			//String firstv = prop.getProperty("1.iter.1.value");
			//System.out.println(first);
			//System.out.println(firstv);
			int i =1;
			
			//HashMap<Sensor_Type, Message> hm = new HashMap<>();
			prop.forEach((key,value) -> map.put((String) key,(String) value));
			//System.out.println(map.toString());
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			inputStream.close();
		}
		
		return map;
	}
}
