package org.sensorsimulation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ReadProperties {
	public static void main(String[] args) throws IOException {
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String,String> map = new HashMap<>();
		map = properties.getPropValues();
		System.out.println(map.toString());
	}
}
