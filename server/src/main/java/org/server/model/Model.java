package org.server.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class Model {
	 private Connection cn = null;
	 public PreparedStatement preparedUpdateSt, preparedDeleteSt, preparedSelectSt, preparedInsertSt;
	 
	 public Model (Connection connection) {
		 this.cn = connection;
		 
	 }
	 public boolean insert (String table, String labels, String values) {
			try {
				preparedInsertSt = cn.prepareStatement("INSERT INTO ? (?) VALUES (?) ");
				preparedInsertSt.setString(1, table);
				preparedInsertSt.setString(2, labels);	
				preparedInsertSt.setString(3, values);
				preparedInsertSt.executeUpdate();
				preparedInsertSt.close();
				return true;
			}catch(Exception e) {return false;}
			 
	}
	 
     public boolean update (String table, String label, String valeur, String labelCondition, String valueCondition) {
         try {
        	 	preparedUpdateSt = cn.prepareStatement("UPDATE ? SET ? = ? WHERE ? = ?");
        	 	preparedUpdateSt.setString(1, table);
        	 	preparedUpdateSt.setString(2, label);
        	 	preparedUpdateSt.setString(3, valeur);
        	 	preparedUpdateSt.setString(4, labelCondition);
        	 	preparedUpdateSt.setString(5,valueCondition);
    			preparedUpdateSt.executeUpdate();
    			preparedUpdateSt.close();
    			return true;
    		}catch(Exception e) {return false;}
    		 
    }
     
     public boolean delete (String table, String labelCondition, String valeurCondition) {
         try {
        	 preparedDeleteSt = cn.prepareStatement("DELETE FROM ? WHERE ? = ? ");
        	 preparedDeleteSt.setString(1, table);
        	 preparedDeleteSt.setString(2, labelCondition);
        	 preparedDeleteSt.setString(3, valeurCondition);
        	 preparedDeleteSt.executeUpdate();
        	 preparedDeleteSt.close();
        	 return true;
    		}catch(Exception e) {return false;}
    		 
    }
     
     
	
	 
     public ResultSet select (String table, String information) {
    	 try {
    		 preparedSelectSt = cn.prepareStatement("SELECT ? FROM ? ");
    		 preparedSelectSt.setString(1, table);
    		 preparedSelectSt.setString(2, information);
			 ResultSet rs = preparedSelectSt.executeQuery();
			 System.out.println(true);
			 preparedSelectSt.close();
			 return rs;
		}catch(Exception e) {return null;}
    	 
		 
	 }
     
	public ResultSet select(String table) {
		// TODO Auto-generated method stub
		return null;
	}

}
