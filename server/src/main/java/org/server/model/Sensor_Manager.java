package org.server.model;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.server.connection.pool.DataSource;

import entities.Sensor;

public class Sensor_Manager {
	private Sensor s= new Sensor();
	
	private Connection con;

	/**
	 * @param con
	 */
	public Sensor_Manager(Connection con) {
		super();
		this.con = con;
	}

	public boolean insert(Sensor sensor) throws SQLException {
		Statement st = con.createStatement();
		String request = "insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,localisation,price,installation_date,intalled,scope) values " + 
				"('"+sensor.getIp_address()+"','"+ sensor.getMac_address()+"', "+ sensor.getState()+", "+ sensor.getFk_type()
				+","+ sensor.getFk_room()+",'"+ sensor.getLocation()+"',"+ sensor.getPrice()+", "+ sensor.getInstallation_date()
				+","+ sensor.isInstalled()+", "+ sensor.getScope()+");";
		
		int n=0;
		try {
		n = st.executeUpdate(request);}
		finally {
			if(st!= null)
				try {
					st.close();
				}catch(Exception e){e.printStackTrace();}
			DataSource.freeConnection(con);
		}
		
		
		return n==1;
	}
	public boolean update(Sensor sensor) throws SQLException {
		Statement st = con.createStatement();
		String request = "update Sensor set  ip_address = '"+sensor.getIp_address()+"',mac_address= '"+sensor.getMac_address()+"',state = "+ sensor.getState()+",fk_type ="+ sensor.getFk_type()+" ,fk_room= "+ sensor.getFk_room()+",localisation= '"+sensor.getLocation()+"',price = "+sensor.getPrice()+",installation_date= "+sensor.getInstallation_date()+",intalled = "+sensor.isInstalled()+",scope = "+sensor.getScope()+" ";
		
		int n=0;
		try {
			n = st.executeUpdate(request);
		}
		finally {
			if(st!= null)
				try {
					st.close();
				}catch(Exception e){e.printStackTrace();}
			DataSource.freeConnection(con);
		}
		return n==1;
	}
	
	public Sensor select(Sensor sensor) {return new Sensor();}
	
	public ArrayList<Sensor>selectAll(Sensor sensor) {return new ArrayList<Sensor>();}
	
	public boolean delte(Sensor sendsor) {
		int n=0;
		return n==1;
	}

}
