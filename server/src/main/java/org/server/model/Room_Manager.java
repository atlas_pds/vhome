package org.server.model;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.server.connection.pool.DataSource;

import entities.Room;
import entities.Sensor;

public class Room_Manager {
	
		private Sensor s= new Sensor();

		private Connection con;

		/**
		 * @param con
		 */
		public Room_Manager(Connection con) {
			super();
			this.con = con;
		}
		public boolean insert(Room room) throws SQLException {
			Statement st = con.createStatement();
			String request = "insert into room (type,name,floor,m2,wing) values " + 
					"('"+room.getType()+"','"+ room.getName()+"', '"+ room.getFloor()+"', "+ room.getM2()+", '"+room.getWing()+"');";
			
			int n=0;
			try {
			n = st.executeUpdate(request);}
			finally {
				if(st!= null)
					try {
						st.close();
					}catch(Exception e){e.printStackTrace();}
				DataSource.freeConnection(con);
			}
			
			
			return n==1;
		}
		public boolean update(Room room) throws SQLException {
			Statement st = con.createStatement();
			String request = "update room set  type = '"+room.getType()+"',name= '"+room.getName()+"',floor = '"+ room.getFloor()+"',m2 ="+ room.getM2()+", wing = "+ room.getWing()+"";
			
			int n=0;
			try {
				n = st.executeUpdate(request);
			}
			finally {
				if(st!= null)
					try {
						st.close();
					}catch(Exception e){e.printStackTrace();}
				DataSource.freeConnection(con);
			}
			return n==1;
		}
		
		public Sensor select(Sensor sensor) {return new Sensor();}
		
		public ArrayList<Sensor>selectAll(Sensor sensor) {return new ArrayList<Sensor>();}
		
		public boolean delte(Sensor sendsor) {
			int n=0;
			return n==1;
		}


	}



