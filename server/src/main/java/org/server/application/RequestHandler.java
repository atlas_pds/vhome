package org.server.application;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.channels.NonWritableChannelException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.commons.SensorType;
import org.server.connection.pool.DataSource;
import org.server.model.Model;

import dao.DAOException;
import dao.SensorDAOImpl;
import entities.Historical;
import entities.Client;
import entities.Request;
import entities.Room;
import entities.Sensor;
import entities.Sensor_Type;
import serialization.SerializationDeserialization;

public class RequestHandler implements Runnable {
	
	private final DataInputStream is; 
    private final DataOutputStream os; 
    private final Socket sck;
    private final Connection cn;
    private Model model;
	
	public RequestHandler(Socket sck, Connection cn) throws IOException {
		super();
		this.cn=cn;
		this.sck = sck;
		this.is = new DataInputStream(sck.getInputStream());
		this.os = new DataOutputStream(sck.getOutputStream());
		model = new Model(cn);
	}

	
	public void run() {
		System.err.println("Lancement du traitement de la connexion cliente");

	    boolean closeConnexion = false;
	    //tant que la connexion est active, on traite les demandes
	    System.out.println(!sck.isClosed());
	    do{
			try {
				
				//int result = 0;
				String inputFile = is.readUTF();
				if(inputFile.startsWith("it's time to close the socket"))
					break;
				System.out.println(inputFile);
				SensorDAOImpl sdaoi = new SensorDAOImpl();
	            Request inputRequest = new Request();
				inputRequest = SerializationDeserialization.deserialization((String) inputFile);
				//System.out.println(inputRequest);
				Request outputRequest = new Request();
				outputRequest.setSuccess(Boolean.FALSE);
				/*JsonReader reader = Json.createReader(new StringReader((String) inputFile));
				JsonObject object = reader.readObject();
				System.out.println(object.getString("instruction"));*/
				Statement sta = cn.createStatement();
				switch(inputRequest.getRequest()) {
				
				case "select-sensors" : 
					System.out.println("I take the case: select-sensors");
					
					//sdaoi.select(inputRequest.getConditionLabelValue(), cn);
					sta = cn.createStatement();
					String condition = inputRequest.getConditionLabelValue();
					ResultSet ri=null;
					if(condition.contains("-"))
						ri = sta.executeQuery("select * from Sensor");
					
					else
						ri = sta.executeQuery("select * from sensor inner join sensor_type on sensor.fk_type = sensor_type.id where sensor_type.type = '"+condition+"' ");
					int cont=0;
					while(ri.next())
						cont++;
					os.writeInt(cont);
					
					int k=0;
					
					ri.close();
					sta.close();
					
					
					Statement stati = cn.createStatement();
					
					ResultSet re = null;
					if(condition.contains("-"))
						re = stati.executeQuery("select * from Sensor");
					else
						re = stati.executeQuery("select * from sensor inner join sensor_type on sensor.fk_type = sensor_type.id where sensor_type.type = '"+condition+"' ");
					
					//TODO change setters and add instruction in label condition value
					while(re.next()){
						Request requestSensor = new Request();
						requestSensor.setRequest("sensor");
						Sensor sen = new Sensor();
						sen.setIp_address(re.getString("ip_address"));
						sen.setState(re.getString("state"));
						sen.setInstallation_date(re.getDate("installation_date").toString());
						sen.setInstalled(re.getBoolean("installed"));
						sen.setScope(re.getInt("scope"));
						sen.setMac_address(re.getString("mac_address"));
						sen.setFk_room( Integer.toString(re.getInt("fk_room")));
						sen.setLocation(re.getString("location"));
						sen.setThreshold_min(re.getInt("threshold_min"));//TODO
						sen.setThreshold_max(re.getInt("threshold_max"));
						sen.setPrice(re.getDouble("price"));

						
						
						requestSensor.setObject(sen);
						requestSensor.setSuccess(true);
						String req =SerializationDeserialization.serialization(requestSensor);
						os.writeUTF(req);
					}
					
					
					outputRequest.setObject("");
					outputRequest.setRequest("no-more-client");
					outputRequest.setSuccess(true);
					
					break;
				case "select-sensor-poly" :
					System.out.println("I take the case: select-sensor-poly ");
					String room = inputRequest.getConditionLabelValue();
					sta = cn.createStatement();
					ResultSet resultPoly1 = sta.executeQuery("select * from sensor inner join sensor_type on sensor.fk_type = sensor_type.id inner join room on sensor.fk_room = room.id where room.type = '"+room+"' ");
					int ligns=0;
					while(resultPoly1.next()) {
						ligns++;
					}
					os.writeInt(ligns);
					//resultPoly1.absolute(0);
					ResultSet resultPoly = sta.executeQuery("select * from sensor inner join sensor_type on sensor.fk_type = sensor_type.id inner join room on sensor.fk_room = room.id where room.type = '"+room+"' ");					
					while(resultPoly.next()){
						
						Request requeteclient = new Request();
						Sensor s = new Sensor();
						
						s.setFk_type(resultPoly.getString("type"));
						s.setState(resultPoly.getString("state"));
						requeteclient.setObject(s);
						requeteclient.setSuccess(true);
 
						String req =SerializationDeserialization.serialization(requeteclient);
						os.writeUTF(req);
					}
					resultPoly1.close();
					resultPoly.close();
					sta.close();
					outputRequest.setSuccess(true);
					break;
				case "select-number of sensor" : 
					System.out.println("I take the case: select-number of sensor ");
					String type = inputRequest.getConditionLabelValue();
					//sdaoi.select(inputRequest.getConditionLabelValue(), cn);
					sta = cn.createStatement();
					ResultSet result = sta.executeQuery("select * from sensor inner join sensor_type on sensor.fk_type = sensor_type.id where sensor_type.type = '"+type+"' ");					int nbSensor=0;
					while(result.next())
						nbSensor++;
					outputRequest.setLabel(Integer.toString(nbSensor));
					outputRequest.setSuccess(Boolean.TRUE);
					
					break;
					
				case "select-sensorScope" : 
					System.out.println("I take the case: select-sensorScope");
					String ty = inputRequest.getLabelValue();
					System.out.println(ty);
					sta = cn.createStatement();
					ResultSet result1 = sta.executeQuery("select * from sensor inner join sensor_type on sensor.fk_type = sensor_type.id where sensor_type.type = '"+ty+"' ");					
					int cont1=0;
					while(result1.next())
						cont1++;
					//
					System.out.println(cont1);
					os.writeInt(cont1);
					//System.out.println(result1);
					result1.close();
					sta.close();
					
					
					Statement st2 = cn.createStatement();
					
					ResultSet re1 = st2.executeQuery("select * from sensor inner join sensor_type on sensor.fk_type = sensor_type.id where sensor_type.type = '"+ty+"' ");
					
					while(re1.next()){
						
						Request requeteclient = new Request();
						Sensor s = new Sensor();
						
						s.setPrice(re1.getDouble("price"));
						s.setScope(re1.getInt("scope"));
						requeteclient.setObject(s);
						requeteclient.setSuccess(true);
 
						String req =SerializationDeserialization.serialization(requeteclient);
						os.writeUTF(req);
					}
					
					outputRequest.setObject("");
					
					break;
					
				case "select-number-of-room" : 
					System.out.println("I take the case: select-number-of-room ");
					
					//sdaoi.select(inputRequest.getConditionLabelValue(), cn);
					sta = cn.createStatement();
					ResultSet resul = sta.executeQuery("select * from room");
					int nbRoom=0;
					while(resul.next())
						nbRoom++;
					outputRequest.setLabel(Integer.toString(nbRoom));
					outputRequest.setSuccess(Boolean.TRUE);
					
					break;
					
				case "select-all-clients" : 
					System.out.println("I take the case: select-all-clients");
					
					//sdaoi.select(inputRequest.getConditionLabelValue(), cn);
					sta = cn.createStatement();
					ResultSet rsu = sta.executeQuery("select * from client");
					int nbS=0;
					while(rsu.next())
						nbS++;
					os.writeInt(nbS);
										
					rsu.close();
					sta.close();
					
					
					sta = cn.createStatement();
					
					ResultSet resultS = sta.executeQuery("select * from client");
					
					while(resultS.next()){
						Request requeteclient = new Request();
						requeteclient.setRequest("client");
						Client client = new Client();
						
						client.setFirst_name(resultS.getString("first_name"));
						client.setLast_name(resultS.getString("last_name"));
						
						client.setBirthday(resultS.getDate("birthday"));
						client.setAdress(resultS.getString("adress"));
						client.setEmergency_nb(resultS.getString("emergency_nb"));
						
						client.setGender(resultS.getString("gender"));
						client.setRoom_nb( resultS.getInt("room_nb"));
						
						requeteclient.setObject(client);
						requeteclient.setSuccess(true);
 
						String req =SerializationDeserialization.serialization(requeteclient);
						os.writeUTF(req);
					}
					
					
					outputRequest.setObject("");
					outputRequest.setRequest("no-more-client");
					outputRequest.setSuccess(true);
					
					break;
					
				case "select-number-of-client" : 
					System.out.println("I take the case: select-number-of-client ");
					
					//sdaoi.select(inputRequest.getConditionLabelValue(), cn);
					sta = cn.createStatement();
					ResultSet res = sta.executeQuery("select * from client");
					int nbClient=0;
					while(res.next())
						nbClient++;
					outputRequest.setLabel(Integer.toString(nbClient));
					outputRequest.setSuccess(Boolean.TRUE);
					
					break;
					
				case "select-number-of-alert" : 
					System.out.println("I take the case: select-number-of-alert ");
					
					//sdaoi.select(inputRequest.getConditionLabelValue(), cn);
					Statement ss = cn.createStatement();
					ResultSet res1 = ss.executeQuery("select * from historical WHERE date > current_date - integer '30'");
					int nbAlert=0;
					while(res1.next())
						nbAlert++;
					outputRequest.setLabel(Integer.toString(nbAlert));
					outputRequest.setSuccess(Boolean.TRUE);
					
					break;
	
				case "select-quantity-sensor" : 
					System.out.println("I take the case: select-quantity-sensor");
					sta = cn.createStatement();
					ResultSet reslt = sta.executeQuery("select * from sensor");
					int quantitySensor=0;
					while(reslt.next())
						quantitySensor++;
					outputRequest.setLabel(Integer.toString(quantitySensor));
					outputRequest.setSuccess(Boolean.TRUE);
					
					break;
						
					
				case "insert-sensor" : Sensor sensor = new Sensor();
										System.out.println("I take the case: insert-sensor");
										sensor = SerializationDeserialization.deserialization(inputRequest.getObject(), sensor);
										
										//System.out.println(sensor.getId());
										
										sdaoi.insert(sensor, cn);
										outputRequest.setSuccess(Boolean.TRUE);
					/*PreparedStatement preparedInsertSt;
					try {
						//ce qui va suivre est une arnaque monumentale
						preparedInsertSt = cn.prepareStatement("INSERT INTO Sensor (fk_type, location) VALUES ("+sensor.getFk_type()+", '"+sensor.getLocation()+"')" );
						System.out.println(preparedInsertSt.executeUpdate());
						preparedInsertSt = cn.prepareStatement("INSERT INTO Center (name, adress , city, capacity, phone_number ) VALUES ('StSimon', 'chez Rakibi', 'DelSierra', 1, '118 218')");
						System.out.println(preparedInsertSt.executeUpdate());
					} catch (SQLException e) {
						
						e.printStackTrace();
					}*/
					break;
					
				case "indicator-sensors" : 
					System.out.println("I take the case: indicator-sensors");
					
					//sdaoi.select(inputRequest.getConditionLabelValue(), cn);
					sta = cn.createStatement();
					condition = inputRequest.getConditionLabelValue();
					ri=null;
					ri = sta.executeQuery("select sensor.ip_address, sensor.mac_address, sensor.state, sensor.fk_type, sensor.fk_room, sensor.location, sensor.price, sensor.installation_date, sensor.installed, sensor.scope, sensor.threshold_min, sensor.threshold_max, sensor_type.type,sensor.fk_room from sensor,sensor_type WHERE sensor_type.id = sensor.fk_type "+condition);//TODO
					cont=0;
					while(ri.next())
						cont++;
					os.writeInt(cont);
					k=0;
					
					ri.close();
					sta.close();
					
					
					Statement stati1 = cn.createStatement();
					
					ResultSet res11 = null;
					res11 = stati1.executeQuery("select sensor.ip_address, sensor.mac_address, sensor.state, sensor.fk_type, sensor.fk_room, sensor.location, sensor.price, sensor.installation_date, sensor.installed, sensor.scope, sensor.threshold_min, sensor.threshold_max, sensor_type.type,sensor.fk_room from sensor,sensor_type WHERE sensor_type.id = sensor.fk_type "+condition);//TODO
					
					
					while(res11.next()){
						Request requestSensor = new Request();
						requestSensor.setRequest("sensor");
						Sensor sen = new Sensor();
						sen.setIp_address(res11.getString("ip_address"));
						sen.setState(res11.getString("state"));
						sen.setInstallation_date(res11.getDate("installation_date").toString());
						sen.setInstalled(res11.getBoolean("installed"));
						sen.setScope(res11.getInt("scope"));
						sen.setMac_address(res11.getString("mac_address"));
						sen.setFk_room( Integer.toString(res11.getInt("fk_room")));
						sen.setLocation(res11.getString("location"));
						sen.setThreshold_min(res11.getInt("threshold_min"));//TODO
						sen.setThreshold_max(res11.getInt("threshold_max"));
						sen.setPrice(res11.getDouble("price"));
						sen.setType(res11.getString("type"));

						
						
						requestSensor.setObject(sen);
						requestSensor.setSuccess(true);
						String req =SerializationDeserialization.serialization(requestSensor);
						os.writeUTF(req);
					}
					
					
					outputRequest.setObject("");
					outputRequest.setRequest("no-more-client");
					outputRequest.setSuccess(true);
					
					break;
				case "indicator-room" : 
					System.out.println("I take the case: indicator-room");
					sta = cn.createStatement();
					ri=null;
					ri = sta.executeQuery("select * from room");
					cont=0;
					while(ri.next())
						cont++;
					os.writeInt(cont);
					k=0;
					ri.close();
					sta.close();
					stati1 = cn.createStatement();
					res11 = null;
					res11 = stati1.executeQuery("select * from room");
					while(res11.next()){
						Request requestSensor = new Request();
						requestSensor.setRequest("sensor_type");
						Room sen = new Room();
						
						sen.setType(res11.getString("type"));
						sen.setId(res11.getInt("id"));
						sen.setName(res11.getString("name"));
						sen.setM2(res11.getInt("m2"));
						
						sta = cn.createStatement();
						ri=null;
						ri = sta.executeQuery("select * from sensor where sensor.fk_room = "+sen.getId());
						cont=0;
						while(ri.next())
							cont++;
						sen.setFloor(Integer.toString(cont));

						
						
						requestSensor.setObject(sen);
						requestSensor.setSuccess(true);
						String req =SerializationDeserialization.serialization(requestSensor);
						os.writeUTF(req);
					}
					
					
					outputRequest.setObject("");
					outputRequest.setRequest("no-more-client");
					outputRequest.setSuccess(true);
					
					break;
					
				case "indicator-alert" : 
					System.out.println("I take the case: indicator-alert");
					condition = inputRequest.getConditionLabelValue();
					sta = cn.createStatement();
					ri=null;
					ri = sta.executeQuery("select historical.date, historical.error_type, historical.fk_sensor,sensor.mac_address,room.type from historical,sensor,room "
							+ "where historical.fk_sensor = sensor.id and sensor.fk_room = room.id "+condition);
					cont=0;
					while(ri.next())
						cont++;
					os.writeInt(cont);
					k=0;
					ri.close();
					sta.close();
					stati1 = cn.createStatement();
					res11 = null;
					res11 = stati1.executeQuery("select historical.date, historical.error_type, historical.fk_sensor,sensor.mac_address,room.type from historical,sensor,room where historical.fk_sensor = sensor.id and sensor.fk_room = room.id "+condition);
					
					int a=0;
					while(res11.next()){
						Request requestSensor = new Request();
						requestSensor.setRequest("Alerts");
						Historical sen = new Historical();
						
						sen.setDate(res11.getDate("date"));

						sen.setMac_address(res11.getString("mac_address"));

						sen.setError_type(res11.getString("error_type"));
						System.out.println(a++);
						sen.setRoom(res11.getString("type"));						
						System.out.println(a++);
						requestSensor.setObject(sen); 
						requestSensor.setSuccess(true);
						String req =SerializationDeserialization.serialization(requestSensor);
						os.writeUTF(req);
					}
					
					
					outputRequest.setObject("");
					outputRequest.setRequest("no-more-client");
					outputRequest.setSuccess(true);
					
					break;	
				case "indicator-typeSensors" :  
					System.out.println("I take the case: indicator-typeSensors");
					
					//sdaoi.select(inputRequest.getConditionLabelValue(), cn);
					sta = cn.createStatement();
					ri=null;
					ri = sta.executeQuery("select * from sensor_type");
					cont=0;
					while(ri.next())
						cont++;
					os.writeInt(cont);
					k=0;
					
					ri.close();
					sta.close();
					
					
					stati1 = cn.createStatement();
					
					res11 = null;
					res11 = stati1.executeQuery("select * from sensor_type");
					
					
					while(res11.next()){
						Request requestSensor = new Request();
						requestSensor.setRequest("sensor_type");
						Sensor_Type sen = new Sensor_Type();
						
						sen.setType(res11.getString("type"));
						sen.setAlerts_nb(res11.getInt("alerts_nb"));
						sen.setSensibility_max(res11.getInt("sensibility_max"));//TODO
						sen.setSensibility_min(res11.getInt("sensibility_min"));//TODO


						
						
						requestSensor.setObject(sen);
						requestSensor.setSuccess(true);
						String req =SerializationDeserialization.serialization(requestSensor);
						os.writeUTF(req);
					}
					
					
					outputRequest.setObject("");
					outputRequest.setRequest("no-more-client");
					outputRequest.setSuccess(true);
					
					break;
	
				case "select-sensor-by_type" : System.out.println("I take the case: select-sensor-by_type");
					Sensor sensorSelect = new Sensor();
					sensorSelect = SerializationDeserialization.deserialization(inputRequest.getObject(), sensorSelect);
												sdaoi.select(sensorSelect.getType(), cn);
												outputRequest.setSuccess(Boolean.TRUE);
												
				case "delete-sensor" : System.out.println("I take the case: delete-sensor");
					Sensor sensorDelete = new Sensor();
					String label = "type";
					sensorDelete = SerializationDeserialization.deserialization(inputRequest.getObject(), sensorDelete);
										sdaoi.delete(cn, label, sensorDelete.getType());
										outputRequest.setSuccess(Boolean.TRUE);
				case "update-sensor" :
					System.out.println("I take the case: update-sensor");
					Sensor sens = new Sensor();
					sens = SerializationDeserialization.deserialization(inputRequest.getObject(), sens);
					
						outputRequest.setSuccess(sdaoi.update(sens, cn));

					
					outputRequest.setObject("");
					break;
				default : 
					outputRequest.setSuccess(Boolean.FALSE);
					
				case "select-sensors-not-configured":
                   System.out.println("I take the case: select-sensors-not-configured ");
					
					//sdaoi.select(inputRequest.getConditionLabelValue(), cn);
					Statement sc = cn.createStatement();
					ResultSet res2 = sc.executeQuery("select * from sensor WHERE fk_type = 1");
					int nbc=0;
					while(res2.next())
						nbc++;
					outputRequest.setLabel(Integer.toString(nbc));
					outputRequest.setSuccess(Boolean.TRUE);
					
					break;
										
				}
			
				String outputFile = SerializationDeserialization.serialization(outputRequest);
				os.writeUTF(outputFile);
				
			} catch (SocketException e) {
				e.printStackTrace();
				break;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (DAOException e) {
				System.out.println("fail!");
			}catch (SQLException e) {
				System.out.println("fail during the statement");
				Request fail = new Request();
				fail.setSuccess(Boolean.FALSE);;
			}
	    }
	    while(!sck.isClosed());
		DataSource.freeConnection(cn);
		System.out.println("Client is disconnected");
	}

	
}
