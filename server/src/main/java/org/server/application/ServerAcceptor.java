package org.server.application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.util.Properties;

import org.server.connection.pool.Config;
import org.server.connection.pool.DataSource;

public class ServerAcceptor {
	
	
	private static Connection connection;
	//Properties p = new Properties();
	
	//On initialise des valeurs par défaut
	   private int port;
	   private ServerSocket server = null;
	   private boolean isRunning = true;
	   
	   public ServerAcceptor(){
		  port= Config.getPort();
	      try {
	         server = new ServerSocket(port);
	      } catch (UnknownHostException e) {
	         e.printStackTrace();
	      } catch (IOException e) {
	         e.printStackTrace();
	      }
	   }
	   
	   
	   //On lance notre serveur
	   public void open(){
	      
	      //Toujours dans un thread à part vu qu'il est dans une boucle infinie
	      Thread t = new Thread(new Runnable(){
	         public void run(){
	            while(isRunning == true){
	               
	               try {
	            	  connection = DataSource.getConnection();
	            	  System.out.println(connection.getSchema());
	            	  System.out.println(connection.getMetaData().getURL());
	                  //On attend une connexion d'un client
	                  Socket client = server.accept();
	                  
	                  //Une fois reçue, on la traite dans un thread séparé
	                  System.out.println("Client is connected !");
	                  Thread t = new Thread(new RequestHandler(client, connection));
	                  t.start();
	                  
	               } catch (Exception e) {
	            	   if (e.getMessage().equals("wait")) {
	   					continue;
	   				}
	                  e.printStackTrace();
	               }
	            }
	            
	            try {
	               server.close();
	            } catch (IOException e) {
	               e.printStackTrace();
	               server = null;
	            }
	         }
	      });
	      
	      t.start();
	   }
	   
	   public void close(){
	      isRunning = false;
	   }   
}

