 
create table Center ( 
     id serial not null, 
     name char(25) not null, 
     adress char(50) not null, 
     city char(25) not null, 
     capacity numeric(1) not null, 
     phone_number char(15) not null, 
     primary key (id)); 
  
create table Users ( 
     id serial not null, 
     function varchar(25), 
     last_name char(25) not null, 
     first_name char(25) not null, 
     birthday date not null, 
     gender char(10) not null, 
     username char(10) not null, 
     password char(15) not null, 
     primary key (id)); 
  
create table Client ( 
     id serial not null, 
     last_name char(25) not null, 
     first_name char(25) not null, 
     birthday date not null, 
     adress char(50) not null,  
     room_nb numeric(4) not null, 
     emergency_nb char(15) not null, 
     gender char(10) not null, 
     primary key (id)); 
  
create table Room ( 
      
     id serial not null, 
     type varchar(25), 
     name varchar(25) not null, 
     floor numeric(2) not null, 
     m2 integer not null, 
     wing varchar(25) not null, 
     primary key (id)); 
 
 create table Sensor_type ( 
     id serial not null, 
     type varchar(25) not null, 
     sensibility_min integer, 
     sensibility_max integer, 
     alerts_nb integer, 
     primary key (id)); 
 
create table Sensor ( 
     id serial not null, 
     ip_address varchar(25) , 
     mac_address varchar(25) unique, 
     state varchar(25), 
     fk_type integer not null, 
     fk_room integer, 
     location varchar(20) , 
     price float , 
     installation_date Date, 
     installed boolean, 
     scope integer, 
     threshold_min integer, 
    threshold_max integer, 
x integer, 
y integer, 
     primary key (id), 
     foreign key (fk_type) references Sensor_type(id), 
     foreign key (fk_room) references Room (id)); 
  
create table Historical ( 
     id serial not null, 
     date date, 
     error_type char(25) not null, 
     fk_sensor integer, 
     primary key (id), 
     foreign key (fk_sensor) references Sensor(id)); 
      
    
/* 
create table Sensor_state ( 
     id serial not null, 
     name char(25) not null, 
     constraint ID primary key (id_etat));*/ 
  
      
  
  
-- Constraints Section 
-- _______  
  /*create trigger*/

 CREATE OR REPLACE FUNCTION changeMax()
  RETURNS trigger AS
$$
BEGIN
         IF NEW.threshold_max > (Select sensibility_max FROM sensor_type WHERE sensor_type.id = NEW.fk_type ) THEN
             UPDATE sensor_type SET sensibility_max = NEW.threshold_max WHERE sensor_type.id = NEW.fk_type;
         END IF;
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER changeMaxSensibilityTrigger
  AFTER INSERT OR UPDATE
  ON sensor
  FOR EACH ROW
  EXECUTE PROCEDURE changeMax();


 CREATE OR REPLACE FUNCTION changeMin()
  RETURNS trigger AS
$$
BEGIN
         IF NEW.threshold_min < (Select sensibility_max FROM sensor_type WHERE sensor_type.id = NEW.fk_type ) THEN
             UPDATE sensor_type SET sensibility_min = NEW.threshold_min WHERE sensor_type.id = NEW.fk_type;
         END IF;
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER changeMinSensibilityTrigger
  AFTER INSERT OR UPDATE
  ON sensor
  FOR EACH ROW
  EXECUTE PROCEDURE changeMin();
  
------------------------------------------------

 CREATE OR REPLACE FUNCTION alertManager()
  RETURNS trigger AS
$$
BEGIN
    IF(NEW.state = 'alert') THEN
        IF (TG_OP = 'INSERT') THEN
             UPDATE sensor_type SET alerts_nb = alerts_nb+1 WHERE sensor_type.id = NEW.fk_type;
             INSERT INTO historical (date, error_type,fk_sensor) VALUES (NOW(),'alert',NEW.id);
        
        ELSIF (TG_OP = 'UPDATE') THEN
            UPDATE sensor_type SET alerts_nb = alerts_nb+1 WHERE sensor_type.id = NEW.fk_type;
            INSERT INTO historical (date, error_type,fk_sensor) VALUES (NOW(),'alert',NEW.id);
            
        END IF;
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER alertManagerTrigger
  AFTER INSERT OR UPDATE
  ON sensor
  FOR EACH ROW
  EXECUTE PROCEDURE alertManager();

  
-- Index Section 
-- _____ 
  
  
  
insert into Sensor_type(type,sensibility_min,sensibility_max,alerts_nb) values ('smoke',null, 10,0); 
insert into Sensor_type(type,sensibility_min,sensibility_max,alerts_nb) values('temperature',10, 22,0); 
insert into Sensor_type(type,sensibility_min,sensibility_max,alerts_nb) values('light',0, 500,0); 
insert into Sensor_type(type,sensibility_min,sensibility_max,alerts_nb) values('move',0,15,0); 
 
 
 
 
insert into Room (type,name,floor,m2,wing) values ('kitchen','1A',0,25,'North'); 
insert into Room (type,name,floor,m2,wing) values ('bathroom','2C',0,15,'North'); 
insert into Room (type,name,floor,m2,wing) values ('hallway','1A',0,31,'North'); 
insert into Room (type,name,floor,m2,wing) values ('common room','1A',0,43,'North'); 
insert into Room (type,name,floor,m2,wing) values ('reception','1A',0,24,'North'); 
 
  
insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values  
('192.168.0.2',' 5E:FF:56:A2:AF:15', 'off',  (select id from Sensor_type where type ='smoke'),5,'North',50, '2019-05-08',false, 10,0,25); 
 insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values  
('192.168.0.2','5E:FF:56:A2:AF:16', 'on', (select id from Sensor_type where type ='smoke'),2,'North',50, '2019-05-08',true, 10,0,40); 
insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values  
('192.168.0.2','5E:FF:56:A2:AF:17', 'off', (select id from Sensor_type where type ='smoke'),3,'North',50, '2019-05-08',false, 10,0,25); 
insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values  
('192.168.0.2','5E:FF:56:A2:AF:18', 'on', (select id from Sensor_type where type ='smoke'),4,'North',50, '2019-05-08',true, 10,0,25); 
 
insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values  
('192.168.0.2','5E:FF:56:A2:AF:19','off', (select id from Sensor_type where type ='temperature'),1,'North',50, '2019-05-08',false, 10,100,500); 
insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values  
('192.168.0.2','5E:FF:56:A2:AF:20 ', 'alert', (select id from Sensor_type where type ='temperature'),2,'North',50, '2019-05-08',true, 10,17,27); 
insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values  
('192.168.0.2','5E:FF:56:A2:AF:21','on',  (select id from Sensor_type where type ='temperature'),3,'North',50, '2019-05-08',true, 10,17,27); 
insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values  
('192.168.0.2','5E:FF:56:A2:AF:22', 'alert',  (select id from Sensor_type where type ='temperature'),4,'North',50, '2019-05-08',true, 10,17,30); 
insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values  
('192.168.0.2','5E:FF:56:A2:AF:23', 'off', (select id from Sensor_type where type ='temperature'),5,'North',50, '2019-05-08',false, 10,17,27); 
 
insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values   
('192.168.0.2','5E:FF:56:A2:AF:24', 'on', (select id from Sensor_type where type ='light'),1,'South',50, '2019-05-08',true, 10,100,500); 
  
 insert into Sensor (ip_address,mac_address,state,fk_type,fk_room,location,price,installation_date,installed, scope,threshold_min,threshold_max) values    
 ('192.168.0.2','5E:FF:56:A2:AF:25', 'off', (select id from Sensor_type where type ='move'),1,'South',50, '2018-06-08',false, 10,5,10000);  
 
 
 
 
 
 
 
 
insert into Users(function, last_name,first_name,birthday,gender,username,password) values ('agent','Dupont','Jean','1990-05-06','Male','admin','admin'); 
  
 
/* insert client*/ 
 
 
INSERT INTO client( 
    last_name, first_name, birthday, adress, room_nb, emergency_nb, gender) 
    VALUES ('Petrov', 'Andrei', '1997-09-25','5 rue hubert creteil', 102, 15,'homme'); 
  
  
 INSERT INTO client( 
    last_name, first_name, birthday, adress, room_nb, emergency_nb, gender) 
    VALUES ('Meunier', 'Lucas', '1998-01-12','100 rue alois couilly', 101, 0647367483,'homme'); 
 
 INSERT INTO client( 
    last_name, first_name, birthday, adress, room_nb, emergency_nb, gender) 
    VALUES ('Emam', 'Mohamed', '1998-06-11','7 Boulevard de l epinette Maison Alfort', 101, 0654921555,'homme'); 
 
  
 
 
 /*insert alert*/ 
/* INSERT INTO historical( 
    date, error_type, fk_sensor) 
    VALUES ( '2019-05-13','hs', (select id from sensor where mac_address = '5E:FF:56:A2:AF:15')); 
 
 
 INSERT INTO historical( 
    date, error_type, fk_sensor )
    VALUES ( '2019-05-10','alert', (select id from sensor where mac_address = '5E:FF:56:A2:AF:20')); 
 
 */

