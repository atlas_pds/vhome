package org.gui.app;

import org.gui.controller.FrameController;

import org.gui.controller.ControllerAddSensor;

import org.gui.controller.ControllerConfigCapter;
import org.gui.controller.ControllerPCost;
import org.gui.controller.ControllerPMenu;
import org.gui.view.Fenetre;
//import org.gui.view.PConfigSensor;

public class Main {
	//static PConfigSensor pc = new PConfigSensor();
	public static Fenetre f= new Fenetre();

	public static void main(String[] args) {
		new FrameController(f);
		new ControllerAddSensor(f);
		new ControllerConfigCapter(f);
		new ControllerPCost(f);
		
	}

}
