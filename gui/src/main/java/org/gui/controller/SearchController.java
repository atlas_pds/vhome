package org.gui.controller;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.gui.view.PIndicator;

import entities.Request;

public class SearchController{
	protected Request request = new Request();
	protected JPanel northPanel;

	protected JPanel contentPane;
	JFrame fr;
	protected JPanel panel1;
	
	
	
	public SearchController() {
		if(PIndicator.searchIsOpen)
			return;//only one frame
		PIndicator.searchIsOpen = true;
		fr = new JFrame("Search frame");
		fr.setLocationRelativeTo(null);
		fr.setSize(700, 500);
		fr.setVisible(true);
		fr.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				PIndicator.searchIsOpen = false;		
			}	
		});
		contentPane = (JPanel) fr.getContentPane();
		contentPane.setBackground(Color.LIGHT_GRAY);
		CardLayout cardL = new CardLayout();
		contentPane.setLayout(cardL);
		
		
		//first panel
		panel1 = new JPanel();
		JComboBox<String> tableBox = new JComboBox<String>();
		tableBox.addItem("Sensor");
		tableBox.addItem("Room");
		tableBox.addItem("Sensor type");
		tableBox.addItem("Employee");
		tableBox.addItem("Client");
		tableBox.addItem("Alert");

		panel1.add(tableBox);
		JButton next1 = new JButton("Next");
		
		panel1.add(next1);
		contentPane.add(panel1,"panel1");
		
		
		//second panel
		JPanel panel2 = new JPanel();
		JComboBox<String> labelBox = new JComboBox<String>();
		
		
		next1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				labelBox.removeAllItems();
				switch ((String)tableBox.getSelectedItem()) {						/**choice list**/
				case "Sensor":
					labelBox.addItem("Mac address");
					labelBox.addItem("Scope");
					labelBox.addItem("Installation date");
					labelBox.addItem("State");
					labelBox.addItem("Location");//room+location
					break;
				case "Sensor type":
					labelBox.addItem("Name");
					labelBox.addItem("Sensibility");
					break;
				case "Room":
					labelBox.addItem("Surface");
					labelBox.addItem("Name");
					break;
				case "Employee":
					labelBox.addItem("Informations");
					break;
				case "Client":
					labelBox.addItem("Information");
					break;
				case "Alert":
					labelBox.addItem("Sensors");
					break;
				default:
					break;
				}
				cardL.next(contentPane);

			}
		});
		
		
		
		panel2.add(labelBox);
		//next button
		JButton next2 = new JButton("Next");
		next2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//TODO implement different searches made by the previous page
				cardL.show(contentPane, "panel1");
				
			}
		});
		//back button
		JButton previous2 = new JButton("Previous");
		previous2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cardL.previous(contentPane);
				
			}
		});
		
		panel2.add(previous2);
		panel2.add(next2);
		contentPane.add(panel2,"panel2");

	}
		

}
