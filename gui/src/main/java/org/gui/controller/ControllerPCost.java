package org.gui.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import org.gui.view.Fenetre;
import org.gui.view.PAppli;
import org.gui.view.PCost;

import entities.Request;
import entities.Sensor;
import serialization.SerializationDeserialization;

public class ControllerPCost implements ActionListener{
	//
	private PCost pCost;
	private PAppli pa;
	static Socket sck;
	static DataOutputStream dos;
	static DataInputStream dis;
	private String  resultRequest;
	private double totalCost = 0;

	
	
	public ControllerPCost(Fenetre f) {
		this.pCost = f.getAp().getPu().getCost();
		this.pa = f.getAp();
		pa.getPu().getCost().getAdd().addActionListener(this);
		pa.getPu().getCost().getCalculate().addActionListener(this);
		pa.getPu().getCost().getDelete().addActionListener(this);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		
		
		if(e.getSource().equals(pa.getPu().getCost().getCalculate())) {
			if (!pCost.getJtfName1().getText().isEmpty() && !pCost.getJtfSurface1().getText().isEmpty() && !pCost.getSensorTypeScrollingMenu1().getSelectedItem().equals("choice of Sensor Type")) {
				if (isInteger(pCost.getJtfSurface1().getText())) {
					if(!pCost.getJtfSurface1().getText().equals("0")) {
						try {
							sck = SocketRequestor.getSck();
							dos = SocketRequestor.getDos();
							dis = SocketRequestor.getDis();
							Request rq = new Request();
							
							rq.setRequest("select-sensorScope");
							rq.setLabel("type");
							rq.setLabelValue(pCost.getSensorTypeScrollingMenu1().getSelectedItem().toString());
							String request = SerializationDeserialization.serialization(rq);
							dos.writeUTF(request);
							
							
							int[] scope = new int[10];
							double []price = new double[10];
							int k = 0;
							
							k = SocketRequestor.getDis().readInt();
							
							for(int j = 0; j < k ; j++) {
								String inputFile = SocketRequestor.getDis().readUTF();
								Request getSensor = new Request();
								getSensor = SerializationDeserialization.deserialization(inputFile);
								
								Sensor sensor = new Sensor();
								sensor = SerializationDeserialization.deserialization(getSensor.getObject(),sensor);
								price [j] = sensor.getPrice(); 
								scope [j] = sensor.getScope();
							}
							
							SocketRequestor.getDis().readUTF();
							
							double realPrice = 0;
							int index = 0;
							for (int i =0; i < k - 1;i++) {
								realPrice = price[0];
								if (price[i]>price[i+1])
									realPrice = price[i+1];
									index = i+1;
							}//
							
							
							int finalScope = scope[index];
							int numberOfSensor = Integer.parseInt(pCost.getJtfSurface1().getText())/finalScope;
							int  realInstallationPrice = 75;
							pCost.getJtfSum().setText(String.valueOf(numberOfSensor *( realPrice + realInstallationPrice)));
						
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}else {
						JOptionPane.showMessageDialog(null, "surface can't be null", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}else {
					JOptionPane.showMessageDialog(null, "wrong type", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}else {
				JOptionPane.showMessageDialog(null, "One or more fields are empty", "Error", JOptionPane.ERROR_MESSAGE);
			}
		} 
		
		
			
		if(e.getSource().equals(pCost.getAdd())) {
			
			if (!pCost.getJtfName1().getText().isEmpty() && !pCost.getJtfSurface1().getText().isEmpty() && !pCost.getJtfSum().getText().isEmpty() && !pCost.getSensorTypeScrollingMenu1().getSelectedItem().equals("choice of Sensor Type")) {	
				if (isInteger(pCost.getJtfSurface1().getText()) && isDouble(pCost.getJtfSum().getText())) {
					String name = pCost.getJtfName1().getText();
					String surface = pCost.getJtfSurface1().getText();
					String roomCost = pCost.getJtfSum().getText();
					String typeSensor = String.valueOf(pCost.getSensorTypeScrollingMenu1().getSelectedItem());
					
					int i = 0;
					while (pCost.getRoom()[i] != null) {
						i++;
					}
					pCost.getModel().insertElementAt(name + " - " + surface + " m² " + " - " + typeSensor +  " : " + roomCost , i);
					totalCost = totalCost + Double.parseDouble(pCost.getJtfSum().getText());
					pCost.getJtfTotalSum().setText(String.valueOf(totalCost));
				}else {
					JOptionPane.showMessageDialog(null, "wrong type", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}else {
				JOptionPane.showMessageDialog(null, "One or more fields are empty", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
			
		
		if (e.getSource().equals(pCost.getDelete())) {
			if (!pCost.getJtfName2().getText().isEmpty() && !pCost.getJtfSurface2().getText().isEmpty() && !pCost.getSensorTypeScrollingMenu2().getSelectedItem().equals("choice of Sensor Type")) {
				if (isInteger(pCost.getJtfSurface2().getText())) {
					if (!pCost.getJtfSurface2().getText().equals("0")) {
						String name = pCost.getJtfName2().getText();
						String surface = pCost.getJtfSurface2().getText();
						String typeSensor = String.valueOf(pCost.getSensorTypeScrollingMenu2().getSelectedItem());
						for (int i =0; i < pCost.getModel().getSize() - 1; i++ ) {
							if (pCost.getModel().getElementAt(i).startsWith(name + " - " + surface + " m² " + " - " + typeSensor +  " : " ) == true) {
								String [] price =  pCost.getModel().getElementAt(i).split(": ");
								double cost = Double.parseDouble(price[1]);
								totalCost = Double.parseDouble(pCost.getJtfTotalSum().getText());
								totalCost = totalCost - cost;
								pCost.getJtfTotalSum().setText(String.valueOf(totalCost));
								pCost.getModel().removeElementAt(i);
							}
						}
					}else {
						JOptionPane.showMessageDialog(null, "surface can't be null", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}else {
					JOptionPane.showMessageDialog(null, "wrong type", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}else {
				JOptionPane.showMessageDialog(null, "One or more fields are empty", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}
	private boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	private boolean isDouble(String s) {
		try {
			Double.parseDouble(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
