package org.gui.controller;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import javax.swing.JOptionPane;

import org.gui.app.Main;

public class SocketRequestor {
	static Socket sck = null;
	static DataOutputStream dos;
	static DataInputStream dis;
	
	public static void createSocket() {
		try {
			//better practice: create file properties to get address
			sck = new Socket(InetAddress.getLocalHost(), 2009);//192.168.20.25
			dos = new DataOutputStream (sck.getOutputStream());
			dis = new DataInputStream (sck.getInputStream());	
			
			
		} catch (IOException e) {
			System.out.println("Fail during socket instance");
			 Main.f.getCd().show(Main.f.getContentPane(),"login");
			 JOptionPane.showMessageDialog(null, "Impossible to connect to server", "Error 401", JOptionPane.ERROR_MESSAGE);
		}
			
	}

	public static Socket getSck() {
		return sck;
	}

	public static DataOutputStream getDos() {
		return dos;
	}

	public static DataInputStream getDis() {
		return dis;
	}
	
	public static void closeSocket() {
		if(SocketRequestor.getSck()!=null) {
			try {
				dos.writeUTF("it's time to close the socket");
				sck.close();
				System.out.println("Client socket is closed");
			} catch (IOException e) {
				System.out.println("Impossible to close socket");
				e.printStackTrace();
			}
		}
	}

}
