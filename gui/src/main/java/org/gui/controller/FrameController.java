package org.gui.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import org.gui.view.Fenetre;


public class FrameController implements ActionListener {

	private Fenetre f;
	
	public FrameController(Fenetre frame) {
		this.f = frame;
		f.getPl().getJlogin().addActionListener(this);
		f.getPl().getJlogin().setForeground(Color.RED);


		f.getPl().setBackground(Color.BLUE);

		f.getPl().setBackground(Color.RED);


		f.getPl().setBackground(Color.WHITE);

		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String [] form = new String[2];
		form [0] = f.getPl().getUser().getText();
		form[1] = new String (f.getPl().getjPass().getPassword());
		//System.out.println(form[0] + form[1]);
		
		if(form[0].isEmpty() || form[1].isEmpty()) {
			JOptionPane.showMessageDialog(null, "The entry is empty", "Insertion Error", JOptionPane.ERROR_MESSAGE);
			
			
		}
		else if (new String(f.getPl().getjPass().getPassword()).equals(new String("admin")) && f.getPl().getUser().getText().equals(new String("admin")) && e.getSource()== f.getPl().getJlogin()) {
			//app view
			f.getCd().next(f.getContentPane());
			SocketRequestor.createSocket();
		}
		
		else if (new String(f.getPl().getjPass().getPassword()).equals(new String("local")) && f.getPl().getUser().getText().equals(new String("local")) && e.getSource()== f.getPl().getJlogin()) {
			//app view
			f.getCd().next(f.getContentPane());
		}
		
		else if (new String(f.getPl().getjPass().getPassword()).equals(new String("financial")) && f.getPl().getUser().getText().equals(new String("financial")) && e.getSource()== f.getPl().getJlogin()) {
			//app view
			f.getCd().next(f.getContentPane());
			f.getCd().next(f.getContentPane());
			SocketRequestor.createSocket();

		}
		
		else {
			if (!new String(f.getPl().getjPass().getPassword()).equals(new String("admin"))) {
				JOptionPane.showMessageDialog(null, "false password", "Insertion Error", JOptionPane.ERROR_MESSAGE);
			}
			else if (!f.getPl().getUser().getText().equals(new String("admin"))) {
				JOptionPane.showMessageDialog(null, "false user", "Insertion Error", JOptionPane.ERROR_MESSAGE);
			}
		}
			
		
			
		
	}
	

}
