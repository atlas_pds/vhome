package org.gui.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import org.gui.view.Fenetre;
import org.gui.view.PAddSensor;

import entities.Request;
import entities.Sensor;
import serialization.SerializationDeserialization;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;


public class ControllerAddSensor implements ActionListener {
	private Fenetre f;
	private PAddSensor padd;
	private String  [] tab = new String [4];
	
	
	public ControllerAddSensor(Fenetre f) {
		this.f = f;
		this.padd = f.getAp().getPu().getAddSensor();
		padd.getJbAdd().addActionListener(this);
		
	}
	
public void actionPerformed(ActionEvent e) {
		
		tab[0] = padd.getJtMac().getText();
		tab [1] = padd.getJtPrice().getText();
		tab[2] = padd.getJtScope().getText();
		tab[3] = padd.getCbType().getSelectedItem().toString();
		
		System.out.println(tab.toString());
		System.out.println(tab[0]);
		System.out.println(tab[1]);
		
		if(tab[0].isEmpty() || tab[1].isEmpty() || tab[2].isEmpty()||tab[3].isEmpty())			
			try {
				throw new Exception("A value must be specified for update");
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, "all values must be filled", "Insertion Error", JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			}
		
		if(!tab[2].isEmpty()) {
			if(!isInteger(tab[2]))
				try {
					throw new Exception("Scope must be a Integer");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "please enter an integer on the location scope", "Insertion Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
		}
		
		
		if(tab[0].isEmpty()) {
				try {
					throw new Exception("Mac address is a null");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "please enter your Mac ADDRESS", "Insertion Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
		}
		
		Sensor sensor = new Sensor();
		try {
			Request requestSensor = new Request();
			requestSensor.setRequest("select-sensorScope");
			
			//Connection cn = DriverManager.getConnection(org.server.connection.pool.Config.getUrl(), org.server.connection.pool.Config.getUser(), org.server.connection.pool.Config.getPassword());
			
			sensor.setMac_address(tab[0]);;
			sensor.setPrice(Integer.parseInt(tab[1]));
			sensor.setScope(Integer.parseInt(tab[2]));;
			sensor.setType(tab[3]);
			requestSensor.setObject(sensor);
			String request = SerializationDeserialization.serialization(requestSensor);
			//System.out.println(request);
			SocketRequestor.getDos().writeUTF(request);
			String imputFile = SocketRequestor.getDis().readUTF();
			Request answer = new Request();
			answer = SerializationDeserialization.deserialization(imputFile);
			System.out.println(answer.getObject());
			if(answer.getSuccess()){
				JOptionPane.showMessageDialog(null, "successful configuration", "Insertion message", JOptionPane.INFORMATION_MESSAGE);
			}
			
			//System.out.println("json send me the information: there are "+nb+" sensors");
		} catch (IOException e1) {
			e1.printStackTrace();}		
		
		
		System.out.println(tab[0]);
		if(e.getSource().equals(padd.getJbAdd()));
		System.out.println("vous avez ajouté un capteur");
			}
	private boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}	
}
