package org.gui.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.gui.view.Fenetre;
import org.gui.view.PAppli;
import org.gui.view.PIndicator;
import org.gui.view.PMenu;
import org.gui.view.PUseCase;

public class ControllerPMenu implements ActionListener {
	private PMenu menu;
	private PUseCase pUse;
	private PAppli pa;
	private Fenetre f;


	
	
	public ControllerPMenu(PMenu pm, PAppli pa, Fenetre f) {
		this.menu = pm;
		this.pa = pa;
		this.f = f;
		menu.getMap().addActionListener(this);
		menu.getConfig_sensor().addActionListener(this);
		menu.getIndicator().addActionListener(this);
		menu.getAdd_sensor().addActionListener(this);
		menu.getNeed().addActionListener(this);
		menu.getDeconnexion().addActionListener(this);
	}

	 
		public void actionPerformed(ActionEvent e) {
			//System.out.println(e);
			//System.out.println(e.getActionCommand());
			//System.out.println(e.getSource());
			//System.out.println(e.getSource().equals(f.getAp().getPm().getMap()));
			if(e.getSource().equals(pa.getPm().getMap())) {
				//System.out.println(e.getSource());
				pa.getPu().setCard("Map");
			}
			if(e.getSource().equals(pa.getPm().getConfig_sensor())) {
				pa.getPu().getCl().show(pa.getPu(),"ConfigSensor");
			}
			if(e.getSource().equals(pa.getPm().getIndicator())) {
				pa.getPu().getCl().show(pa.getPu(),"Indicator");
				
			}
			if(e.getSource().equals(pa.getPm().getAdd_sensor())) {
				pa.getPu().getCl().show(pa.getPu(),"AddSensor");
			}
			if(e.getSource().equals(pa.getPm().getNeed())) {
				pa.getPu().getCl().show(pa.getPu(),"Cost");
			}
			if(e.getSource().equals(pa.getPm().getDeconnexion())) {
			 f.getCd().show(f.getContentPane(),"login");
			 SocketRequestor.closeSocket();
			
			}
			
			
		}

}


