package org.gui.controller;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import entities.Client;
import entities.Request;
import entities.Sensor;
import serialization.SerializationDeserialization;

public class ControllerSelectedSensorListener implements ActionListener {
	JComboBox comboBox;
	private static JFrame fSensor;
	private static boolean openWindow;
	
	
	public ControllerSelectedSensorListener(JComboBox<String> comboBox) {
		super();
		this.comboBox = comboBox;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(openWindow) {
			return;
		}
		fSensor = new JFrame("Sensor");
		fSensor.getContentPane();
		fSensor.setLocationRelativeTo(null);
		fSensor.setSize(700, 500);
		fSensor.addWindowListener(new WindowListener() {
			//useless but need
			@Override
			public void windowOpened(WindowEvent e) {}
			@Override
			public void windowIconified(WindowEvent e) {}
			@Override
			public void windowDeiconified(WindowEvent e) {}
			@Override
			public void windowDeactivated(WindowEvent e) {}
			@Override
			public void windowClosed(WindowEvent e) {}
			@Override
			public void windowActivated(WindowEvent e) {}
			
			//interesting one
			@Override
			public void windowClosing(WindowEvent e) {
				openWindow = false;
					
			}	
			
		});
		fSensor.setLayout(new GridLayout(20, 1));
		try {
			Request requestGetSensor = new Request();
			requestGetSensor.setRequest("select-sensors");
			requestGetSensor.setObject("");
			requestGetSensor.setConditionLabelValue((String) comboBox.getSelectedItem());
			String request = SerializationDeserialization.serialization(requestGetSensor);
			SocketRequestor.getDos().writeUTF(request);
			//boolean instruction=true;
			int numberOfSensor=0;
			numberOfSensor=SocketRequestor.getDis().readInt();
			JPanel annouceLabel = new JPanel();
			annouceLabel.setBackground(Color.GRAY);
			annouceLabel.setLayout(new GridLayout(1,11));
			annouceLabel.add(new JLabel("Ip address"));
			annouceLabel.add(new JLabel("mac addresse"));
			annouceLabel.add(new JLabel("State"));
			annouceLabel.add(new JLabel("Room"));
			annouceLabel.add(new JLabel("Location "));
			annouceLabel.add(new JLabel("Price"));
			annouceLabel.add(new JLabel("Installation date"));
			annouceLabel.add(new JLabel("Installed"));
			annouceLabel.add(new JLabel("Scope"));
			annouceLabel.add(new JLabel("Threshold_min"));
			annouceLabel.add(new JLabel("Threshold_max"));

			
			fSensor.add(annouceLabel);
			for (int i = 0; i < numberOfSensor; i++) {
				
			
				String imputFile = SocketRequestor.getDis().readUTF();
				Request getSensor = new Request();
				getSensor=SerializationDeserialization.deserialization(imputFile);
				
	
				
				Sensor sens = new Sensor();
				sens = SerializationDeserialization.deserialization(getSensor.getObject(), sens);
				JPanel jpSensor = new JPanel();
				if(i%2 ==1)
					jpSensor.setBackground(Color.LIGHT_GRAY);
				jpSensor.setLayout(new GridLayout(1,11));
				
				jpSensor.add(new JLabel(sens.getIp_address()));
				jpSensor.add(new JLabel(sens.getMac_address().trim()));
				jpSensor.add(new JLabel(sens.getState().trim()));
				jpSensor.add(new JLabel(sens.getFk_room().trim()));
				jpSensor.add(new JLabel(sens.getLocation().trim()));
				jpSensor.add(new JLabel(Double.toString(sens.getPrice()).trim()));
				jpSensor.add(new JLabel(sens.getInstallation_date().trim()));
				jpSensor.add(new JLabel(Boolean.toString(sens.isInstalled()).trim()));
				jpSensor.add(new JLabel(Integer.toString(sens.getScope()).trim()));
				jpSensor.add(new JLabel(Integer.toString(sens.getThreshold_max()).trim()));
				jpSensor.add(new JLabel(Integer.toString(sens.getThreshold_min()).trim()));

				
				
				fSensor.add(jpSensor);
				
			}
			SocketRequestor.getDis().readUTF();
			//System.out.println("ça boucle avec "+f_name);

		} catch (IOException ed) {
			ed.printStackTrace();
		}
		
		fSensor.setVisible(true);
		openWindow = true;
		
	}

	

}
