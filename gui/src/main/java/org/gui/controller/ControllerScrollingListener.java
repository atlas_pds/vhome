package org.gui.controller;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;

import javax.swing.JLabel;

import entities.Request;
import serialization.SerializationDeserialization;

public class ControllerScrollingListener implements ItemListener {
	private final JLabel numberOfSensor;

	public ControllerScrollingListener(JLabel numberOfSensor) {
		this.numberOfSensor = numberOfSensor;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		String caller = (String) e.getItem();
		String nbSensorType = getSensorTypeNb(caller);
		numberOfSensor.setText(nbSensorType);
		
	}

	public static String getSensorTypeNb(String caller) {
		String nb=null;
		if(caller!="--") {
			try {
				Request requestSensor = new Request();
				requestSensor.setRequest("select-number of sensor");
				requestSensor.setConditionLabel("type");
				requestSensor.setConditionLabelValue(caller);
				requestSensor.setObject("");
				String request = SerializationDeserialization.serialization(requestSensor);
				SocketRequestor.getDos().writeUTF(request);
				
				String imputFile = SocketRequestor.getDis().readUTF();
				Request answer = new Request();
				answer = SerializationDeserialization.deserialization(imputFile);
				if(!answer.getSuccess())
					return "  error";
				nb=answer.getLabel();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return nb;
		}
		else {
			return" ";
		}
	}
}
