package org.gui.controller;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gui.view.PIndicator;

import entities.Client;
import entities.Request;
import serialization.SerializationDeserialization;

public class ControllerPIndicator {
	static JLabel labelTarget;
	static JFrame fClient;
	public static boolean openWindow=false;
	public static String getSensorNb() {
		String nb=null;
		try {
			Request requestSensor = new Request();
			requestSensor.setRequest("select-quantity-sensor");
			requestSensor.setObject("");
			String request = SerializationDeserialization.serialization(requestSensor);
			SocketRequestor.getDos().writeUTF(request);
			
			String imputFile = SocketRequestor.getDis().readUTF();
			Request answer = new Request();
			answer = SerializationDeserialization.deserialization(imputFile);
			if(!answer.getSuccess())
				return "  error";
			nb=answer.getLabel();
			System.out.println("json send me the information: there are "+nb+" sensors");
		} catch (IOException e) {
			
			return "  error";
		}
			return nb;
		
	
	}

	public static String getNumberOfRoom() {
			String nb=null;
			try {
				Request requestRoom = new Request();
				requestRoom.setRequest("select-number-of-room");
				requestRoom.setObject("");
				String request = SerializationDeserialization.serialization(requestRoom);
				SocketRequestor.getDos().writeUTF(request);
				
				String imputFile = SocketRequestor.getDis().readUTF();
				Request answer = new Request();
				answer = SerializationDeserialization.deserialization(imputFile);
				if(!answer.getSuccess())
					return "  error";
				nb=answer.getLabel();
				System.out.println("json send me the information: there are "+nb+" room");
			} catch (IOException e) {
				return "  error";
			}
				return nb;
	}

	public static String getNumberOfClient() {
		String nb=null;
		try {
			Request requestClient = new Request();
			requestClient.setRequest("select-number-of-client");
			requestClient.setObject("");
			String request = SerializationDeserialization.serialization(requestClient);
			SocketRequestor.getDos().writeUTF(request);
			
			String imputFile = SocketRequestor.getDis().readUTF();
			Request answer = new Request();
			answer = SerializationDeserialization.deserialization(imputFile);
			if(!answer.getSuccess())
				return "  error";
			nb=answer.getLabel();
			System.out.println("json send me the information: there are "+nb+" client");
		} catch (IOException e) {
			return "  error";
		}
			return nb; 
	}

	public static String getNumberOfAlert() {
		String nb=null;
		try {
			Request requestAlert = new Request();
			requestAlert.setRequest("select-number-of-alert");
			requestAlert.setObject("");
			String request = SerializationDeserialization.serialization(requestAlert);
			SocketRequestor.getDos().writeUTF(request);
			String imputFile = SocketRequestor.getDis().readUTF();
			Request answer = new Request();
			answer = SerializationDeserialization.deserialization(imputFile);
			if(!answer.getSuccess())
				return "  error";
			nb=answer.getLabel();
			System.out.println("json send me the information: there are "+nb+" alerts");
		} catch (IOException e) {
			return "  error";
		}
			return nb; 
	}

	public static void showClient() {
		if(openWindow) {
			return;
		}
		fClient = new JFrame("CLIENTS");
		fClient.getContentPane();
		fClient.setLocationRelativeTo(null);
		fClient.setSize(500, 500);
		fClient.addWindowListener(new WindowListener() {
			//useless but need
			@Override
			public void windowOpened(WindowEvent e) {}
			@Override
			public void windowIconified(WindowEvent e) {}
			@Override
			public void windowDeiconified(WindowEvent e) {}
			@Override
			public void windowDeactivated(WindowEvent e) {}
			@Override
			public void windowClosed(WindowEvent e) {}
			@Override
			public void windowActivated(WindowEvent e) {}
			
			//interesting one
			@Override
			public void windowClosing(WindowEvent e) {
				openWindow = false;
					
			}	
			
		});
		fClient.setLayout(new GridLayout(10, 1));
		try {
			Request requestGetClient = new Request();
			requestGetClient.setRequest("select-all-clients");
			requestGetClient.setObject("");
			String request = SerializationDeserialization.serialization(requestGetClient);
			SocketRequestor.getDos().writeUTF(request);
			boolean instruction=true;
			int numberOfClient=0;
			numberOfClient=SocketRequestor.getDis().readInt();
			JPanel annouceLabel = new JPanel();
			annouceLabel.add(new JLabel("First_name   Last_name   Adress   Emergency_nb   Gender   Birthday   Room_nb"));
			annouceLabel.setBackground(Color.GRAY);
			fClient.add(annouceLabel);
			for (int i = 0; i < numberOfClient; i++) {
				
			
				String imputFile = SocketRequestor.getDis().readUTF();
				Request getClient = new Request();
				getClient=SerializationDeserialization.deserialization(imputFile);
				
	
				
				Client cli = new Client();
				cli = SerializationDeserialization.deserialization(getClient.getObject(), cli);
				JPanel jpClient = new JPanel();
				if(i%2 ==1)
					jpClient.setBackground(Color.LIGHT_GRAY);
				jpClient.setLayout(new FlowLayout());
				
				String f_name = cli.getFirst_name().trim();
				JLabel first_name = new JLabel(f_name);
				jpClient.add(first_name);
				
				jpClient.add(new JLabel(cli.getLast_name().trim()));
				jpClient.add(new JLabel(cli.getAdress().trim()));
				jpClient.add(new JLabel(cli.getEmergency_nb().trim()));
				jpClient.add(new JLabel(cli.getGender().trim()));
				jpClient.add(new JLabel(cli.getBirthday().toString().trim()));
				jpClient.add(new JLabel(Integer.toString(cli.getRoom_nb()).trim()));
				
				
				fClient.add(jpClient);
				
			}
			SocketRequestor.getDis().readUTF();
			//System.out.println("ça boucle avec "+f_name);

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		fClient.setVisible(true);
		openWindow = true;
		
	}

	
}
