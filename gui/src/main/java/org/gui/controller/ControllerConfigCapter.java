package org.gui.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import javax.swing.JOptionPane;

import org.gui.view.Fenetre;
//import org.gui.view.pcSensor;
import org.gui.view.PConfigSensor;

import entities.Request;
import entities.Sensor;
import serialization.SerializationDeserialization;

public class ControllerConfigCapter implements ActionListener {
	private Fenetre f;
	private PConfigSensor pc;
	private String  [] tab = new String [11];
	final static String DATE_FORMAT = "dd-MM-yyyy";
	
	
	public ControllerConfigCapter(Fenetre f) {
		this.f = f;
		this.pc = f.getAp().getPu().getConfig();
		pc.getJbAdd().addActionListener(this);
		
	}


	
public void actionPerformed(ActionEvent e) {
		
		System.out.println(pc.getJtIP().getText());
		tab [0] = pc.getJtIP().getText();
		tab[1] = pc.getJtMac().getText();
		tab [2] = pc.getJtDate().getText();
		tab [3] = pc.getCbInstaled().getSelectedItem().toString();
		tab[4] = (String) pc.getCbLocalisation().getSelectedItem();
		tab[5] = pc.getCbState().getSelectedItem().toString();
		tab[6] = pc.getsensibility_min().getText();
		tab[7] = pc.getSensibility_max().getText();
		System.out.println(tab.toString());
		System.out.println(tab[2]);
		System.out.println(tab[1]);
		
		if(tab[0].isEmpty() || tab[1].isEmpty() || tab[2].isEmpty()||tab[3].isEmpty() || tab[4].isEmpty() || tab[5].isEmpty() || tab[4].isEmpty())			
			try {
				throw new Exception("A value must be specified for update");
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, "all values must be filled", "Insertion Error", JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			}
		
		if(!tab[6].isEmpty()) {
			if(!isInteger(tab[6]))
				try {
					throw new Exception("sensibilty_min must be a Integer");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "please enter an integer on the location sensibility_min", "Insertion Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
		}
		
		if(!tab[7].isEmpty()) {
			if(!isInteger(tab[7]))
				try {
					throw new Exception("sensibilty_max must be a Integer");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "please enter an integer on the location sensibility_max", "Insertion Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
		}
		
		
		if(tab[0].isEmpty()) {
				try {
					throw new Exception(" please enter IP Address");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "please enter null on the location IP ADDRESS", "Insertion Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
		}
		
		if(!tab[1].isEmpty()) {
			if(tab[1].equals("null"))
				try {
					throw new Exception("Mac adress unavailable");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "please enter Mac ADDRESS", "Insertion Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
		}
		if(tab[2].isEmpty()) {
				try {
					throw new Exception("Installation Date unavailable ");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "please enter null on the location Installation Date", "Insertion Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
		}	
		System.out.println(tab[0]);
		if(e.getSource().equals(pc.getJbAdd()));
		System.out.println("Cool");	
		
		Sensor sensor = new Sensor();
		try {
			Request requestSensor = new Request();
			requestSensor.setRequest("update-sensor");
			
			//Connection cn = DriverManager.getConnection(org.server.connection.pool.Config.getUrl(), org.server.connection.pool.Config.getUser(), org.server.connection.pool.Config.getPassword());
			
			sensor.setIp_address(tab[0]);;
			sensor.setState(tab[5]);
			sensor.setLocation(tab[4]);
			if(!isDateValid(tab[2])){
				JOptionPane.showMessageDialog(null, "invalid Date", "Insertion Error", JOptionPane.ERROR_MESSAGE);
			}
			
			/*LocalDate dateDuJour = LocalDate.now();
			if ( date.isBefore(dateDuJour) ) {
			      afficherMessageErreur();
			}
			*/
			/*LocalDate dateJour = LocalDate.now();
			LocalDate dateRenseigner = t.madate;
			if(!dateRenseigner.equals(dateJour)){
			  System.out.println("ce n'est pas aujourd'hui");
			}*/
			
			sensor.setInstallation_date(tab[2]);			
			sensor.setInstalled(Boolean.valueOf(tab[3]).booleanValue());		
			int x = Integer.parseInt(tab[6]);
			int y = Integer.parseInt(tab[7]);
			if(y<x) {
				try {
					throw new Exception("sensibility_max must be greater than sensibility_min");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "sensibility_max must be greater than sensibility_min", "Insertion Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
			}
			else {
				sensor.setThreshold_max(Integer.parseInt(tab[7]));
				sensor.setThreshold_min(Integer.parseInt(tab[6]));
			}
			
			sensor.setMac_address(tab[1]);
			requestSensor.setObject(sensor);
			String request = SerializationDeserialization.serialization(requestSensor);
			//System.out.println(request);
			SocketRequestor.getDos().writeUTF(request);
			String imputFile = SocketRequestor.getDis().readUTF();
			Request answer = new Request();
			answer = SerializationDeserialization.deserialization(imputFile);
			System.out.println(answer.getObject());
			if(answer.getSuccess()){
				JOptionPane.showMessageDialog(null, "successful configuration", "Insertion message", JOptionPane.INFORMATION_MESSAGE);
			}
			else if (!answer.getSuccess()){
				JOptionPane.showMessageDialog(null, "ERROR your request failed", "Insertion Error", JOptionPane.ERROR_MESSAGE);
			}
			
			//System.out.println("json send me the information: there are "+nb+" sensors");
		} catch (IOException e1) {
			e1.printStackTrace();}	
		
		
		Request requestSensor = new Request();
		requestSensor.setRequest("update-sensor");
		
		
	}
	private boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	

	public  boolean isDateValid(String date) {
		final  String DATE_FORMAT = "yyyy-MM-dd";
	        try {
	            DateFormat d = new SimpleDateFormat(DATE_FORMAT);
	            d.setLenient(false);
	            d.parse(date);
	            return true;
	        } catch (ParseException e) {
	            return false;
	        }
	}
	
	public void compareDates(Date date1,Date date2)
    {
        // if you already have date objects then skip 1

        //date object is having 3 methods namely after,before and equals for comparing
        //after() will return true if and only if date1 is after date 2
        if(date1.after(date2)){
            System.out.println("Date1 is after Date2");
        }

        //before() will return true if and only if date1 is before date2
        if(date1.before(date2)){
            System.out.println("Date1 is before Date2");
        }

        //equals() returns true if both the dates are equal
        if(date1.equals(date2)){
            System.out.println("Date1 is equal Date2");
        }

        System.out.println();
    }
	public static void main( String[] args )
    {
        
    }
	public static long ipToLong(InetAddress ip) {
		byte[] octets = ip.getAddress();
		long result = 0;
		for (byte octet : octets) {
			result <<= 8;
			result |= octet & 0xff;
		}
		return result;
	}
}
