package org.gui.applicationTest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.Socket;

import org.commons.SensorType;
import org.gui.view.Inter3;

import entities.Request;
import entities.Sensor;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import serialization.SerializationDeserialization;

public class Controller {
	static Socket sck;
	static DataOutputStream dos;
	static DataInputStream dis;
	private String  resultRequest;
	public Controller() {
		
		resultRequest="false";
		
		Inter3 view = new Inter3();
		
		ActionListener actAdd = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				SensorType sType = new SensorType();
				if (!view.id1.getText().isEmpty() && !view.sensibility1.getText().isEmpty() && !view.type1.getText().isEmpty())
					sType = new SensorType(view.type1.getText(),view.id1.getText(),view.sensibility1.getText(),"insert");
				try {
					String resultRequest = request(sType);
					view.displayResult(resultRequest);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		
		ActionListener actUpdate = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				SensorType sType = new SensorType("update");
				if (!view.id3.getText().isEmpty() && !view.id4.getText().isEmpty()) {
					sType.setId(view.id3.getText());
					sType.setId2(view.id4.getText());
				}else if (!view.sensibility3.getText().isEmpty() && !view.sensibility4.getText().isEmpty()){
					sType.setSensibility(view.sensibility3.getText());
					sType.setSensibility2(view.sensibility4.getText());
				}else if (!view.type3.getText().isEmpty() && !view.type4.getText().isEmpty()) {
					sType.setType(view.type3.getText());
					sType.setType2(view.type4.getText());
				}else {
					view.displayResult("please write a value");
				}
				try {
					String resultRequest = request(sType);
					view.displayResult(resultRequest);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		
		ActionListener actDelete = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				SensorType sType = new SensorType("delete");
				
				if (!view.id2.getText().isEmpty()) {
					sType.setId(view.id2.getText());
				}else if (!view.sensibility2.getText().isEmpty()){
					sType.setSensibility(view.sensibility2.getText());
				}else if (!view.type2.getText().isEmpty()) {
					sType.setType(view.type2.getText());
				}else {
					view.displayResult("please write a value");
				}
				try {
					String resultRequest = request(sType);
					view.displayResult(resultRequest);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		
		ActionListener actSelect = new ActionListener() {
			public void actionPerformed(ActionEvent e){
				
			}
		};
		
		  view.bAdd1.addActionListener(actAdd);
		  view.bDelete1.addActionListener(actDelete);
		  view.bUpdate1.addActionListener(actUpdate);
		  view.bAdd2.addActionListener(actAdd);
		  view.bDelete2.addActionListener(actDelete);
		  view.bUpdate2.addActionListener(actUpdate);
		  view.bSelect.addActionListener(actSelect);
		 
		  connect();
	}
	
	public static void main(String[] args) {
		Controller controller = new Controller();
	}

	public static void connect() {
		try {//send request
			sck = new Socket(InetAddress.getLocalHost(), 2009);//192.168.20.25
			dos = new DataOutputStream (sck.getOutputStream());
			dis = new DataInputStream (sck.getInputStream());
			Sensor sensor = new Sensor();
			//sensor.setSensibility("normal");
			sensor.setState("TEST");
			//sensor.setType("smoke");
			sensor.setLocation("bedroom");
			sensor.setState("good");
			sensor.setIp_address("190;0;0;0");
			sensor.setMac_address("10.10.9.0");
			sensor.setPrice(50);
			sensor.setScope(10);
			sensor.setFk_room("bedroom");
			sensor.setFk_type("smoke");
			Request rq = new Request();
			rq.setRequest("insert-sensor");
			rq.setObject(sensor);
			String request = SerializationDeserialization.serialization(rq);
			dos.writeUTF(request);
			//receive request
			String response = dis.readUTF();
			System.out.println(response);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	protected String request(SensorType sType) throws IOException {
		String json = "bla";
		dos.write(json.getBytes());
		String inputFile = dis.readUTF();
		JsonReader reader = Json.createReader(new StringReader(inputFile));
		JsonObject object = reader.readObject();
		closeConnection();
		return object.getString("success");
		
	}
	private void closeConnection() throws IOException {
		sck.close();
	}

}
