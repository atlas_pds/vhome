package org.gui.view;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JPanel;



import org.gui.controller.ControllerPCost;


import org.gui.view.PAddSensor;
import org.gui.view.PConfigSensor;
import org.gui.view.PCost;
import org.gui.view.PIndicator;
import org.gui.view.PMap;

public class PUseCase extends JPanel {
	private PConfigSensor config;
	private static PMap map;
	private PAddSensor addSensor;
	private PCost cost;
	private PIndicator indicator;
	private PCost pcost;
	private PAppli pa;
	private CardLayout cl;
	
	
	public PUseCase() {
		super();
		config = new PConfigSensor();
		map = new PMap();
		addSensor = new PAddSensor();
		cost = new PCost();
		indicator = new PIndicator();
		map.setBackground(Color.WHITE);
		config.setBackground(Color.GRAY);
		addSensor.setBackground(Color.GRAY);
		cost.setBackground(Color.GRAY);
		indicator.setBackground(Color.LIGHT_GRAY);
		//ControllerPCost cp = new ControllerPCost(pcost,pa);
		
		cl = new CardLayout();
		this.setLayout(cl);
		
		//this.add(new JButton("Welcome"));
		this.setName("Welcome");
		this.add("Map",map);
		this.add( "ConfigSensor",config);
		this.add("AddSensor",addSensor);
		this.add("Cost",cost);
		this.add("Indicator",indicator);
		//cl.show(map,"Map");
		//this.setPreferredSize(new Dimension(400,400));
						
	}
	public void setCard(String name) {
		cl.show(this, name);
	}


	public  PConfigSensor getConfig() {
		return config;
	}


	public  PMap getMap() {
		return map;
	}


	public PAddSensor getAddSensor() {
		return addSensor;
	}


	public PCost getCost() {
		return cost;
	}


	public PIndicator getIndicator() {
		return indicator;
	}
	
	public CardLayout getCl() {
		return cl;
	}
	
}