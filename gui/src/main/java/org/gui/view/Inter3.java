package org.gui.view;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.commons.SensorType;

public class Inter3 extends JFrame {
	//
	JFrame maFenetre;
	public JButton bAdd1, bAdd2, bDelete1, bDelete2, bUpdate1, bUpdate2,bSelect;
	public JTextField name1,name2,name3, name4,firstName1,firstName2, firstName3, firstName4,type1,type2,type3,type4,id1,id2,id3,id4,sensibility1,sensibility2,sensibility3,sensibility4;
	private JLabel l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,oldName,oldFirstName,newName,newFirstName,oldType,oldId,oldSensibility,newType,newId,newSensibility;
	private JPanel addPanelPatient,updatePanelPatient,deletePanelPatient,menuPanel,addPanelSensorType,updatePanelSensorType,deletePanelSensorType;
	JMenu sensorMenu = new JMenu ("Sensor Type");
	JMenu patientMenu = new JMenu ("Patients");
	JMenuItem item1 = new JMenuItem("Add");
	JMenuItem item2 = new JMenuItem("Delete");
	JMenuItem item3 = new JMenuItem("Update");
	JMenuItem item4 = new JMenuItem("Add");
	JMenuItem item5 = new JMenuItem("Delete");
	JMenuItem item6 = new JMenuItem("Update");
	JMenuItem item7 = new JMenuItem("Select All");
	JMenuBar bar = new JMenuBar();
	JPanel cards = new JPanel(new CardLayout());
	JPanel selectPanelSensorType = new JPanel();
	
	
	ActionListener menuSelectSensor = new ActionListener() {
		public void actionPerformed(ActionEvent e){
		
			CardLayout c1 = (CardLayout)(cards.getLayout());
			c1.show(cards,"SelectPanelSensorType");
		}
	};

	ActionListener menuAddPatient = new ActionListener() {
		public void actionPerformed(ActionEvent e){
		
			CardLayout c1 = (CardLayout)(cards.getLayout());
			c1.show(cards,"AddPanelPatient");
		}
	};
	
	ActionListener menuDeletePatient = new ActionListener() {
		public void actionPerformed(ActionEvent e){
		
			CardLayout c1 = (CardLayout)(cards.getLayout());
			c1.show(cards,"DeletePanelPatient");
		}
	};
	
	ActionListener menuUpdatePatient = new ActionListener() {
		public void actionPerformed(ActionEvent e){
		
			CardLayout c1 = (CardLayout)(cards.getLayout());
			c1.show(cards,"UpdatePanelPatient");
		}
	};
	
	ActionListener menuAddSensor = new ActionListener() {
		public void actionPerformed(ActionEvent e){
		
			CardLayout c1 = (CardLayout)(cards.getLayout());
			c1.show(cards,"addPanelSensorType");
		}
	};
	
	ActionListener menuDeleteSensor = new ActionListener() {
		public void actionPerformed(ActionEvent e){
		
			CardLayout c1 = (CardLayout)(cards.getLayout());
			c1.show(cards,"deletePanelSensorType");
		}
	};
	
	ActionListener menuUpdateSensor = new ActionListener() {
		public void actionPerformed(ActionEvent e){
		
			CardLayout c1 = (CardLayout)(cards.getLayout());
			c1.show(cards,"updatePanelSensorType");
		}
	};
	
	
	
	
	

	public Inter3() {
		setSize(900,900);
		//setTitle("Gestion des patients");
		
		item1.addActionListener(menuAddSensor);
		item2.addActionListener(menuDeleteSensor);
		item3.addActionListener(menuUpdateSensor);
		item4.addActionListener(menuAddPatient);
		item5.addActionListener(menuDeletePatient);
		item6.addActionListener(menuUpdatePatient);
		item7.addActionListener(menuSelectSensor);
		
		sensorMenu.add(item1);
		sensorMenu.add(item2);
		sensorMenu.add(item3);
		sensorMenu.add(item7);
		
		patientMenu.add(item4);
		patientMenu.add(item5);
		patientMenu.add(item6);
		
		bar.add(sensorMenu);
		bar.add(patientMenu);
		
		setJMenuBar(bar);
			
		bAdd1 = new JButton ("Add");
		bDelete1 = new JButton ("Delete");
		bUpdate1 = new JButton ("Update");
		
		bAdd2 = new JButton ("Add");
		bDelete2 = new JButton ("Delete");
		bUpdate2 = new JButton ("Update");
		bSelect = new JButton("Display");
		
		
		name1 = new JTextField();
		firstName1 = new JTextField();
		name2 = new JTextField();
		firstName2 = new JTextField();
		name3 = new JTextField();
		firstName3 = new JTextField();
		name4 = new JTextField();
		firstName4 = new JTextField();
		type1 = new JTextField();
		type2 = new JTextField();
		type3 = new JTextField();
		type4 = new JTextField();
		sensibility1 = new JTextField();
		sensibility2 = new JTextField();
		sensibility3 = new JTextField();
		sensibility4 = new JTextField();
		id1 = new JTextField();
		id2 = new JTextField();
		id3 = new JTextField();
		id4 = new JTextField();
		
		
		
		l1 = new JLabel("fistname");
		l2 = new JLabel("name");
		l3 = new JLabel("fistname");
		l4 = new JLabel("name");
		l5 = new JLabel("type");
		l6 = new JLabel("sensitivity");
		l7 = new JLabel("ID");
		l8 = new JLabel("type");
		l9 = new JLabel("sensitivity");
		l10 = new JLabel("ID");
		
		oldName = new JLabel("old name");
		oldFirstName = new JLabel("old firstname");
		newName = new JLabel("new name");
		newFirstName = new JLabel("new firstname");
		oldType = new JLabel("old type");
		oldSensibility = new JLabel("old sensitivity");
		oldId = new JLabel("old id");
		newType = new JLabel("new type");
		newSensibility = new JLabel("new sensitivity");
		newId = new JLabel("new id");
			
		addPanelPatient = new JPanel();
		updatePanelPatient = new JPanel();
		deletePanelPatient = new JPanel();
		addPanelSensorType = new JPanel();
		updatePanelSensorType = new JPanel();
		deletePanelSensorType = new JPanel();
		menuPanel = new JPanel();
		
		GridBagLayout a  = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		addPanelPatient.setLayout(a);
		
		c.gridx = 0;
		c.gridy = 0;
		addPanelPatient.add(l1,c);
		
		c.gridx = 1;
		c.gridy = 0;
		addPanelPatient.add(l2,c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.3;
		firstName3.setPreferredSize(new Dimension(150,25));
		addPanelPatient.add(firstName3,c);
		
		
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 0.3;
		name3.setPreferredSize(new Dimension(150,25));
		addPanelPatient.add(name3,c);
		
		
		c.gridx = 2;
		c.gridy = 1;
		c.weightx = 0.2;
		c.insets = new Insets(30,30,30,30);
		bAdd1.setPreferredSize(new Dimension(100,25));
		addPanelPatient.add(bAdd1,c);
		
		deletePanelPatient.setLayout(a);
		
		c.gridx = 0;
		c.gridy = 0;
		deletePanelPatient.add(l3,c);
		
		c.gridx = 1;
		c.gridy = 0;
		deletePanelPatient.add(l4,c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.3;
		firstName4.setPreferredSize(new Dimension(150,25));
		deletePanelPatient.add(firstName4,c);
		
		
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 0.3;
		name4.setPreferredSize(new Dimension(150,25));
		deletePanelPatient.add(name4,c);
		
		
		c.gridx = 2;
		c.gridy = 1;
		c.weightx = 0.2;
		c.insets = new Insets(30,30,30,30);
		bDelete1.setPreferredSize(new Dimension(100,25));
		deletePanelPatient.add(bDelete1,c);
		
	
		
		updatePanelPatient.setLayout(a);
		
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.3;
		c.insets = new Insets(30,30,30,30);
		firstName1.setPreferredSize(new Dimension(150,25));
		updatePanelPatient.add(firstName1,c);
		
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 0.3;
		name1.setPreferredSize(new Dimension(150,25));
		updatePanelPatient.add(name1,c);
		
		c.gridx = 0;
		c.gridy = 3;
		c.weightx = 0.3;
		firstName2.setPreferredSize(new Dimension(150,25));
		updatePanelPatient.add(firstName2,c);
		
		c.gridx = 1;
		c.gridy = 3;
		c.weightx = 0.3;
		name2.setPreferredSize(new Dimension(150,25));
		updatePanelPatient.add(name2,c);
		
		c.gridx = 0;
		c.gridy = 0;
		updatePanelPatient.add(oldFirstName,c);
		
		c.gridx = 1;
		c.gridy = 0;
		updatePanelPatient.add(oldName,c);
		
		c.gridx = 0;
		c.gridy = 2;
		updatePanelPatient.add(newFirstName,c);
		
		c.gridx = 1;
		c.gridy = 2;
		updatePanelPatient.add(newName,c);
		
		c.gridx = 1;
		c.gridy = 4;
		bUpdate1.setPreferredSize(new Dimension(100,25));
		updatePanelPatient.add(bUpdate1,c);
		
		
		addPanelSensorType.setLayout(a);
		
		c.gridx = 0;
		c.gridy = 0;
		addPanelSensorType.add(l5,c);
		
		c.gridx = 1;
		c.gridy = 0;
		addPanelSensorType.add(l6,c);
		
		c.gridx = 2;
		c.gridy = 0;
		addPanelSensorType.add(l7,c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.3;
		type1.setPreferredSize(new Dimension(150,25));
		addPanelSensorType.add(type1,c);
		
		
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 0.3;
		sensibility1.setPreferredSize(new Dimension(150,25));
		addPanelSensorType.add(sensibility1,c);
		
		c.gridx = 2;
		c.gridy = 1;
		c.weightx = 0.3;
		id1.setPreferredSize(new Dimension(150,25));
		addPanelSensorType.add(id1,c);
		
		
		c.gridx = 3;
		c.gridy = 1;
		c.weightx = 0.2;
		c.insets = new Insets(30,30,30,30);
		bAdd2.setPreferredSize(new Dimension(100,25));
		addPanelSensorType.add(bAdd2,c);
		
		deletePanelSensorType.setLayout(a);
		
		c.gridx = 0;
		c.gridy = 0;
		deletePanelSensorType.add(l8,c);
		
		c.gridx = 1;
		c.gridy = 0;
		deletePanelSensorType.add(l9,c);
		
		c.gridx = 2;
		c.gridy = 0;
		deletePanelSensorType.add(l10,c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.3;
		type2.setPreferredSize(new Dimension(150,25));
		deletePanelSensorType.add(type2,c);
		
		
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 0.3;
		sensibility2.setPreferredSize(new Dimension(150,25));
		deletePanelSensorType.add(sensibility2,c);
		
		c.gridx = 2;
		c.gridy = 1;
		c.weightx = 0.3;
		id2.setPreferredSize(new Dimension(150,25));
		deletePanelSensorType.add(id2,c);
		
		
		c.gridx = 3;
		c.gridy = 1;
		c.weightx = 0.2;
		c.insets = new Insets(30,30,30,30);
		bDelete2.setPreferredSize(new Dimension(100,25));
		deletePanelSensorType.add(bDelete2,c);
		
		updatePanelSensorType.setLayout(a);
		
		c.gridx = 0;
		c.gridy = 0;
		updatePanelSensorType.add(oldType,c);
		
		c.gridx = 1;
		c.gridy = 0;
		updatePanelSensorType.add(oldSensibility,c);
		
		c.gridx = 2;
		c.gridy = 0;
		updatePanelSensorType.add(oldId,c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.3;
		type3.setPreferredSize(new Dimension(150,25));
		updatePanelSensorType.add(type3,c);
		
		
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 0.3;
		sensibility3.setPreferredSize(new Dimension(150,25));
		updatePanelSensorType.add(sensibility3,c);
		
		c.gridx = 2;
		c.gridy = 1;
		c.weightx = 0.3;
		id3.setPreferredSize(new Dimension(150,25));
		updatePanelSensorType.add(id3,c);
		
		c.gridx = 0;
		c.gridy = 2;
		updatePanelSensorType.add(newType,c);
		
		c.gridx = 1;
		c.gridy = 2;
		updatePanelSensorType.add(newSensibility,c);
		
		c.gridx = 2;
		c.gridy = 2;
		updatePanelSensorType.add(newId,c);
		
		c.gridx = 0;
		c.gridy = 3;
		c.weightx = 0.3;
		type4.setPreferredSize(new Dimension(150,25));
		updatePanelSensorType.add(type4,c);
		
		
		c.gridx = 1;
		c.gridy = 3;
		c.weightx = 0.3;
		sensibility4.setPreferredSize(new Dimension(150,25));
		updatePanelSensorType.add(sensibility4,c);
		
		c.gridx = 2;
		c.gridy = 3;
		c.weightx = 0.3;
		id4.setPreferredSize(new Dimension(150,25));
		updatePanelSensorType.add(id4,c);
		
		
		c.gridx = 3;
		c.gridy = 3;
		c.weightx = 0.2;
		c.insets = new Insets(30,30,30,30);
		bUpdate2.setPreferredSize(new Dimension(100,25));
		updatePanelSensorType.add(bUpdate2,c);
		
		
		selectPanelSensorType.setLayout(a);
		
		c.gridx = 2;
		c.gridy = 0;
		selectPanelSensorType.add(bSelect,c);
		
		cards.add(menuPanel,"Menu");
		cards.add(addPanelPatient,"AddPanelPatient");
		cards.add(updatePanelPatient,"UpdatePanelPatient");
		cards.add(deletePanelPatient,"DeletePanelPatient");
		cards.add(addPanelSensorType,"addPanelSensorType");
		cards.add(updatePanelSensorType,"updatePanelSensorType");
		cards.add(deletePanelSensorType,"deletePanelSensorType");
		cards.add(selectPanelSensorType,"SelectPanelSensorType");
		
		
		
		
		this.getContentPane().add(cards);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		}
		
		public static void main (String[] args) {
			Inter3 i = new Inter3();
			//
		}

		public void displayResult(String resultRequest) {
			// TODO Auto-generated method stub
			
		}
		
	
}

