package org.gui.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.gui.controller.SocketRequestor;

import entities.Request;
import entities.Sensor;
import serialization.SerializationDeserialization;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class FrameNoConfig extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameNoConfig frame = new FrameNoConfig();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameNoConfig() {
		this.setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1163, 653);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		try {
			contentPane.removeAll();
			contentPane.setLayout(new GridLayout(12, 1));
			Request requestGetSensor = new Request();
			requestGetSensor.setRequest("indicator-sensors");
			requestGetSensor.setObject("");
			String condition = "and sensor.ip_address is null ";
			
			requestGetSensor.setConditionLabelValue(condition);
			String request = SerializationDeserialization.serialization(requestGetSensor);
			SocketRequestor.getDos().writeUTF(request);
			//boolean instruction=true;
			int numberOfSensor=0;
			numberOfSensor=SocketRequestor.getDis().readInt();
			JPanel annouceLabel = new JPanel();
			float[] hsb = Color.RGBtoHSB(165, 88, 60, null);
			annouceLabel.setBackground(new Color(86, 191, 235));
			annouceLabel.setForeground(Color.WHITE);
			annouceLabel.setLayout(new GridLayout(0, 12, 0, 0));
			JLabel label_7 = new JLabel(" Ip address");
			annouceLabel.add(label_7);
			JLabel label_8 = new JLabel("Mac addresse");
			annouceLabel.add(label_8);
			JLabel label = new JLabel("Type");
			annouceLabel.add(label);
			JLabel label_3 = new JLabel("State");
			annouceLabel.add(label_3);
			JLabel label_4 = new JLabel("Room");
			annouceLabel.add(label_4);
			JLabel label_2 = new JLabel("Location ");
			annouceLabel.add(label_2);
			JLabel label_11 = new JLabel("Price");
			annouceLabel.add(label_11);
			JLabel label_9 = new JLabel("Installation date ");
			annouceLabel.add(label_9);
			JLabel label_1 = new JLabel(" Installed");
			annouceLabel.add(label_1);
			JLabel label_10 = new JLabel("Scope");
			annouceLabel.add(label_10);
			JLabel label_6 = new JLabel("Threshold_min");
			annouceLabel.add(label_6);
			JLabel label_5 = new JLabel("Threshold_max");
			annouceLabel.add(label_5);
			contentPane.add(annouceLabel);
			for (int i = 0; i < numberOfSensor; i++) {
				
			System.out.println("cool");
				String imputFile = SocketRequestor.getDis().readUTF();
				Request getSensor = new Request();
				getSensor=SerializationDeserialization.deserialization(imputFile);
				
				Sensor sens = new Sensor();
				sens = SerializationDeserialization.deserialization(getSensor.getObject(), sens);
				JPanel jpSensor = new JPanel();
				if(i%2 ==0) {
					
					hsb = Color.RGBtoHSB(192, 247, 246, null);
					jpSensor.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
				}
				else {
					hsb = Color.RGBtoHSB(130, 220, 243, null);
					jpSensor.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
				}
				jpSensor.setLayout(new GridLayout(1,12));
				
				jpSensor.add(new JLabel(" "+sens.getIp_address()));
				jpSensor.add(new JLabel(sens.getMac_address().trim()));
				jpSensor.add(new JLabel(sens.getType().trim()));
				jpSensor.add(new JLabel(sens.getState().trim()));
				jpSensor.add(new JLabel(sens.getFk_room().trim()));
				jpSensor.add(new JLabel(sens.getLocation().trim()));
				jpSensor.add(new JLabel(Double.toString(sens.getPrice()).trim()));
				jpSensor.add(new JLabel(sens.getInstallation_date().trim()));
				jpSensor.add(new JLabel(Boolean.toString(sens.isInstalled()).trim()));
				jpSensor.add(new JLabel(Integer.toString(sens.getScope()).trim()));
				jpSensor.add(new JLabel(Integer.toString(sens.getThreshold_min()).trim()));
				jpSensor.add(new JLabel(Integer.toString(sens.getThreshold_max()).trim()));
				
				
				contentPane.add(jpSensor);
				
			}
			SocketRequestor.getDis().readUTF();
			//System.out.println("ça boucle avec "+f_name);

		} catch (IOException ed) {
			ed.printStackTrace();
		}
	}

}
