package org.gui.view;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PAddSensor extends JPanel {
	
	private JButton jbAdd;
	private JComboBox<String> cbType =new JComboBox<>();
	/*private JComboBox<String> cbState = new JComboBox<>();
	private JComboBox<String> cbLocalisation = new JComboBox<>();
	private JComboBox<String> cbInstaled = new JComboBox<>();
	*/
	
	//private JTextField jtIP = new JTextField();
	private JTextField jtMac = new JTextField();
	//private JTextField jtRoom = new JTextField();
	private JTextField jtPrice = new JTextField();
	//private JTextField jtDate = new JTextField();
	private JTextField jtScope = new JTextField();
	public PAddSensor() {
		super();
		jbAdd = new JButton("ADD SENSOR");
		this.setLayout(new GridLayout(23,1));
		
		//combo type
		cbType.addItem("smoke");
		cbType.addItem("temperature");
		/*cbState.addItem("true");
		cbState.addItem("false");
		cbLocalisation.addItem("North");
		cbLocalisation.addItem("South");
		cbLocalisation.addItem("East");
		cbLocalisation.addItem("West");
		cbInstaled.addItem("true");
		cbInstaled.addItem("false");*/
		//add labels of columns
		/*this.add(new JLabel("IP ADDRESS"));
		this.add(jtIP);*/
		this.add(new JLabel("MAC ADDRESS"));
		this.add(jtMac);
		/*this.add(new JLabel("STATE"));
		this.add(cbState);*/
		this.add(new JLabel("TYPE"));
		this.add(cbType);
		/*this.add(new JLabel("ROOM"));
		this.add(jtRoom);*/
		/*this.add(new JLabel("LOCALISATION"));
		this.add(cbLocalisation);*/
		this.add(new JLabel("PRICE") );
		this.add(jtPrice);
		/*this.add(new JLabel("INSTALLATION DATE"));
		this.add(jtDate);*/
		/*this.add(new JLabel("intalled"));
		this.add(cbInstaled);*/
		this.add(new JLabel("SCOPE"));
		this.add(jtScope);
		
		
		
		this.add(jbAdd);
		
	}
	public JButton getJbAdd() {
		return jbAdd;
	}
	public JComboBox<String> getCbType() {
		return cbType;
	}
	/*public JComboBox<String> getCbState() {
		return cbState;
	}
	public JComboBox<String> getCbLocalisation() {
		return cbLocalisation;
	}
	public JComboBox<String> getCbInstaled() {
		return cbInstaled;
	}
	public JTextField getJtIP() {
		return jtIP;
	}*/
	public JTextField getJtMac() {
		return jtMac;
	}
	/*public JTextField getJtRoom() {
		return jtRoom;
	}*/
	public JTextField getJtPrice() {
		return jtPrice;
	}
	/*public JTextField getJtDate() {
		return jtDate;
	}*/
	public JTextField getJtScope() {
		return jtScope;
	}

	

}


