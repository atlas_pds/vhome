package org.gui.view;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gui.controller.ControllerScrollingListener;
import org.gui.controller.ControllerSelectedSensorListener;
import org.gui.controller.SearchController;
import org.gui.controller.ControllerPIndicator;

public class PIndicator extends JPanel {
	
	public JComboBox scrollingMenu;
	public JLabel numberOfSensor;
	GridLayout g;
	public JLabel labelNbSensorResult;
	public JLabel labelNbRoom;
	public JLabel labelNbClient;
	public JLabel labelNbAlert;
	public JPanel panelGeneral;
	public static boolean searchIsOpen = false;
																		/** Constructor**/
	public PIndicator() {
		
			
		labelNbSensorResult = new JLabel();
		labelNbRoom = new JLabel();
		labelNbClient = new JLabel();
		labelNbAlert = new JLabel();
		panelGeneral = new JPanel();
		g = new GridLayout(6,2);
		//GridLayout lay = new GridLayout(1,2);
		BorderLayout lay = new BorderLayout();
		panelGeneral.setLayout(g);
		this.setLayout(lay);
		setPanelSensor();
		setNbRoom();
		setNbClient();
		setAlert();
		setClient();
		setSearch();
		this.add("Center",panelGeneral );
		JButton jb = new JButton("REFRESH");
		ActionListener actionLi = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refresh();
				}
			};
			jb.addActionListener(actionLi);
		this.add("South",jb);
	}
	
	
	
	private void setSearch() {
		JButton search = new JButton("More information");
		search.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//new SearchController();
				new FrameIndicator();
			}
		});
		this.add("North",search);		
	}
	



	private void setClient() {
		JLabel lShowClient = new JLabel("SEE ALL CLIENTS :");
		panelGeneral.add(lShowClient);
		JPanel panelBut = new JPanel();
		JButton buttonGetclient = new JButton("SHOW");
		buttonGetclient.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ControllerPIndicator.showClient();
			}
		});
		panelBut.add(buttonGetclient);
		panelGeneral.add(panelBut);
	}																
																			
																		



	private void setAlert() {
		JLabel lAlert = new JLabel("NUMBER OF ALERTS IN LAST 30 DAYS :");
		panelGeneral.add(lAlert);
		String nbAlert = "Please refresh";
		labelNbAlert.setText(nbAlert);
		panelGeneral.add(labelNbAlert);
	}



	private void setNbClient() {
		JLabel lClient = new JLabel("NUMBER OF CLIENTS :");
		panelGeneral.add(lClient);
		String nbClient = "Please refresh";
		labelNbClient.setText(nbClient);
		panelGeneral.add(labelNbClient);
	}



	private void setNbRoom() {
		JLabel lroom = new JLabel("NUMBER OF ROOMS :");
		panelGeneral.add(lroom);
		String nbRoom = "Please refresh";
		labelNbRoom.setText(nbRoom);
		panelGeneral.add(labelNbRoom);
	}

	public void setPanelSensor() {
		//Key nbSensor
		JLabel labelNbSensor = new JLabel("NUMBER OF SENSOR :");
														//labelNbSensor.setBackground(Color.GREEN);
		panelGeneral.add(labelNbSensor);
		//value nbSensor
		String nbSensor;
		nbSensor = "Please refresh";
		
												//nbSensor = NbSensorTypeController.getSensorNb();
		
		
		labelNbSensorResult.setText(nbSensor);
		/*GridLayout grid = new GridLayout(2,1);
		pol.setLayout(grid);										//NbSensorTypeController.setLabel(labelNbSensorResult);
		JButton jb = new JButton("Refresh");
		ActionListener actionLi = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refresh();
			}
		};*/
		
		
		
		
		/*jb.addActionListener(actionLi);
		JPanel jlabelbutton = new JPanel();
		jlabelbutton.add(jb);
		pol.add(jlabelbutton);*/
		//pol.add(labelNbSensorResult);
		
		panelGeneral.add(labelNbSensorResult);
															//key typeSensorNb
		JLabel labeltypeNbSensor = new JLabel("NUMBER OF SENSORS OF THE TYPE :");
		panelGeneral.add(labeltypeNbSensor);
		//value typeSensorNb
														//Jpanel contains two conponants
		JPanel panelType = new JPanel();
		panelType.setLayout(new GridLayout(3,1));
		String[] types = {"--","smoke", "light", "move","temperature"};
		scrollingMenu = new JComboBox<>(types);
		panelType.add(scrollingMenu);
		numberOfSensor = new JLabel("Please refresh");
	    													//Add a listener
	    scrollingMenu.addItemListener(new ControllerScrollingListener(numberOfSensor));
	    panelType.add(numberOfSensor);
	    //add possibility to see sensor of each types
	    JButton seeSensor = new JButton("SHOW SENSORS");
	    seeSensor.addActionListener(new ControllerSelectedSensorListener(scrollingMenu));
	    panelType.add(seeSensor);
	    panelGeneral.add(panelType);
	    
		
			
	}


	public void refresh() {
		
		labelNbSensorResult.setText(ControllerPIndicator.getSensorNb());
		labelNbSensorResult.repaint();
		numberOfSensor.setText(ControllerScrollingListener.getSensorTypeNb(scrollingMenu.getSelectedItem().toString()));
		
		labelNbRoom.setText(ControllerPIndicator.getNumberOfRoom());
		labelNbClient.setText(ControllerPIndicator.getNumberOfClient());
		labelNbAlert.setText(ControllerPIndicator.getNumberOfAlert());
		
	}
	
	

}

