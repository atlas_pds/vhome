package org.gui.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PCost extends JPanel {
	
	private JButton add, delete,calculate;
	private JPanel addRoom, deleteRoom,totalCost,RoomList,addDeleteTotal;
	private JLabel name1,name2,surface1,surface2,sum,totalSum;
	private JTextField jtfName1,jtfName2,jtfSurface1,jtfSurface2,jtfSum,jtfTotalSum;
	private GridBagConstraints c = new GridBagConstraints();
	
	int roomSize = 25;
	String [] room = new String[roomSize];
	String [] type = {"choice of Sensor Type","smoke","temperature"};
	JComboBox <String> roomScrollingMenu;
	JComboBox sensorTypeScrollingMenu1 ;
	JComboBox sensorTypeScrollingMenu2 ;
	DefaultComboBoxModel <String> model ;
	
	
	
	public PCost() {
		super();
		
		addDeleteTotal = new JPanel();
		RoomList = new JPanel();
		addRoom = new JPanel();
		deleteRoom = new JPanel();
		totalCost = new JPanel();
		
		
		
		name1 = new JLabel("name");
		name2 = new JLabel("name");
		surface1 = new JLabel("surface");
		surface2 = new JLabel("surface");
		sum = new JLabel("cost");
		totalSum = new JLabel("total cost");
		
		
		jtfName1 = new JTextField();
		jtfName2 = new JTextField();
		jtfSurface1 = new JTextField();
		jtfSurface2 = new JTextField();
		jtfSum = new JTextField();
		jtfTotalSum = new JTextField();
		
		room[0] = "List of Rooms";
		model = new DefaultComboBoxModel<String>(room);
		roomScrollingMenu = new JComboBox <String>(model);
		sensorTypeScrollingMenu1 = new JComboBox(type);
		sensorTypeScrollingMenu2 = new JComboBox(type);
		

		
		
		add = new JButton("Add");
		delete = new JButton("Delete");
		calculate = new JButton("Calculate");
		
		BorderLayout bl = new BorderLayout();
		this.setLayout(bl);
		
		GridLayout b = new GridLayout(3,1);
		addDeleteTotal.setLayout(b);
		
		addDeleteTotal.add(addRoom);
		addDeleteTotal.add(deleteRoom);
		addDeleteTotal.add(totalCost);
		
		this.add(RoomList,bl.EAST);
		this.add(addDeleteTotal,bl.CENTER);
		
		
		
		addRoom.setLayout(new GridBagLayout());
		
		
		c.gridx = 0;
		c.gridy = 0;
		addRoom.add(name1, c);
		
		c.gridx = 1;
		c.gridy = 0;
		addRoom.add(surface1, c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(30,30,20,20);
		jtfName1.setPreferredSize(new Dimension(150,25));
		addRoom.add(jtfName1, c);
		
		c.gridx = 1;
		c.gridy = 1;
		jtfSurface1.setPreferredSize(new Dimension(150,25));
		addRoom.add(jtfSurface1, c);
		
		c.gridx = 2;
		c.gridy = 1;
		c.weightx = 0.3;
		addRoom.add(sensorTypeScrollingMenu1, c);
		
		c.gridx = 0;
		c.gridy = 2;
		calculate.setPreferredSize(new Dimension(150,25));
		addRoom.add(calculate,c);
		
		c.gridx = 1;
		c.gridy = 2;
		addRoom.add(sum, c);
		
		c.gridx = 2;
		c.gridy = 2;
		jtfSum.setPreferredSize(new Dimension(150,25));
		addRoom.add(jtfSum, c);
		
		c.gridx = 3;
		c.gridy = 2;
		add.setPreferredSize(new Dimension(150,25));
		addRoom.add(add, c);
		
		
		
		deleteRoom.setLayout(new GridBagLayout());
		
		c.gridx = 0;
		c.gridy = 0;
		deleteRoom.add(name2, c);
		
		c.gridx = 1;
		c.gridy = 0;
		deleteRoom.add(surface2, c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(30,30,20,20);
		jtfName2.setPreferredSize(new Dimension(150,25));
		deleteRoom.add(jtfName2, c);
		
		c.gridx = 1;
		c.gridy = 1;
		jtfSurface2.setPreferredSize(new Dimension(150,25));
		deleteRoom.add(jtfSurface2, c);
		
		c.gridx = 2;
		c.gridy = 1;
		c.weightx = 0.3;
		deleteRoom.add(sensorTypeScrollingMenu2, c);
		
		c.gridx = 1;
		c.gridy = 2;
		delete.setPreferredSize(new Dimension(150,25));
		deleteRoom.add(delete, c);
		
		
		
		totalCost.setLayout(new GridBagLayout());
		
		c.gridx = 0;
		c.gridy = 0;
		totalCost.add(totalSum, c);	
		
		c.gridx = 1;
		c.gridy = 0;
		jtfTotalSum.setPreferredSize(new Dimension(150,25));
		totalCost.add(jtfTotalSum,c);
		
		RoomList.setLayout(new GridBagLayout());
		
		c.gridx = 0;
		c.gridy = 0;
		RoomList.add(roomScrollingMenu);
		
		RoomList.setBackground(Color.WHITE);
		deleteRoom.setBackground(Color.GRAY);
		RoomList.setPreferredSize(new Dimension(300,350));
		
		
	}

	public JButton getAdd() {
		return add;
	}

	public void setAdd(JButton add) {
		this.add = add;
	}

	public JButton getDelete() {
		return delete;
	}

	public void setDelete(JButton delete) {
		this.delete = delete;
	}

	

	public JTextField getJtfName1() {
		return jtfName1;
	}

	public void setJtfName1(JTextField jtfName1) {
		this.jtfName1 = jtfName1;
	}

	public JTextField getJtfName2() {
		return jtfName2;
	}

	public void setJtfName2(JTextField jtfName2) {
		this.jtfName2 = jtfName2;
	}

	public JTextField getJtfSurface1() {
		return jtfSurface1;
	}

	public void setJtfSurface1(JTextField jtfSurface1) {
		this.jtfSurface1 = jtfSurface1;
	}

	public JTextField getJtfSurface2() {
		return jtfSurface2;
	}

	public void setJtfSurface2(JTextField jtfSurface2) {
		this.jtfSurface2 = jtfSurface2;
	}

	public JTextField getJtfSum() {
		return jtfSum;
	}

	public void setJtfSum(JTextField jtfSum) {
		this.jtfSum = jtfSum;
	}

	public JTextField getJtfTotalSum() {
		return jtfTotalSum;
	}

	public void setJtfTotalSum(JTextField jtfTotalSum) {
		this.jtfTotalSum = jtfTotalSum;
	}

	public JComboBox getRoomScrollingMenu() {
		return roomScrollingMenu;
	}

	public void setRoomScrollingMenu(JComboBox roomScrollingMenu) {
		this.roomScrollingMenu = roomScrollingMenu;
	}

	public JComboBox getSensorTypeScrollingMenu1() {
		return sensorTypeScrollingMenu1;
	}

	public void setSensorTypeScrollingMenu1(JComboBox sensorTypeScrollingMenu1) {
		this.sensorTypeScrollingMenu1 = sensorTypeScrollingMenu1;
	}

	public JComboBox getSensorTypeScrollingMenu2() {
		return sensorTypeScrollingMenu2;
	}

	public void setSensorTypeScrollingMenu2(JComboBox sensorTypeScrollingMenu2) {
		this.sensorTypeScrollingMenu2 = sensorTypeScrollingMenu2;
	}

	public JButton getCalculate() {
		return calculate;
	}

	public void setCalculate(JButton calculate) {
		this.calculate = calculate;
	}

	public String[] getRoom() {
		return room;
	}

	public void setRoom(String[] room) {
		this.room = room;
	}

	public DefaultComboBoxModel<String> getModel() {
		return model;
	}	

}
