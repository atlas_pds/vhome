package org.gui.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageInputStreamImpl;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gui.controller.SocketRequestor;

import entities.Request;
import entities.Sensor;
import serialization.SerializationDeserialization;

public class Map extends JPanel implements MouseListener {
	
	private BufferedImage mapImage;
	private static final Rectangle poly1 = new Rectangle(17, 17, 283, 283);
	private static final Rectangle poly2 = new Rectangle(301, 17, 170, 283);
	private static final Rectangle poly3 = new Rectangle(590, 17, 335, 392);
	private static final Rectangle poly4 = new Rectangle(17, 300, 575, 115);
	private static final Rectangle poly5 = new Rectangle(470, 17, 120, 283);
	private static final Rectangle poly6 = new Rectangle(17, 416, 453, 170);
	
	public Map() {
		try {
			mapImage = ImageIO.read(new File("src/main/resources/vhomemap.PNG"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.addMouseListener(this);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawImage(mapImage, 0, 0, this);
		
		g.drawRect(poly1.x, poly1.y, poly1.width, poly1.height);
		g.drawRect(poly2.x, poly2.y, poly2.width, poly2.height);
		g.drawRect(poly3.x, poly3.y, poly3.width, poly3.height);
		g.drawRect(poly4.x, poly4.y, poly4.width, poly4.height);
		g.drawRect(poly5.x, poly5.y, poly5.width, poly5.height);
		g.drawRect(poly6.x, poly6.y, poly6.width, poly6.height);

	}
	
	private boolean location(Point mouse, Rectangle area) {
		if (area.contains(mouse))
			return true;
		else
			return false;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		Point mouse = e.getPoint();
		System.out.println(location(mouse, poly1));
		if (location(mouse, poly1)) {
			HashMap<String, String> sensors = request("kitchen");
			//JOptionPane jop1 = new JOptionPane();
			JOptionPane.showMessageDialog(null, sensors, "Kitchen", JOptionPane.INFORMATION_MESSAGE);
		}
		if (location(mouse, poly2)) {
			HashMap<String, String> sensors = request("bathroom");
			//JOptionPane jop1 = new JOptionPane();
			JOptionPane.showMessageDialog(null, sensors, "Bathroom", JOptionPane.INFORMATION_MESSAGE);
		}
		if (location(mouse, poly3)) {
			HashMap<String, String> sensors = request("common room");
			//JOptionPane jop1 = new JOptionPane();
			JOptionPane.showMessageDialog(null, sensors, "Common room", JOptionPane.INFORMATION_MESSAGE);
		}
		if (location(mouse, poly4) || location(mouse, poly5)) {
			HashMap<String, String> sensors = request("hallway");
			//JOptionPane jop1 = new JOptionPane();
			JOptionPane.showMessageDialog(null, sensors, "Hallway", JOptionPane.INFORMATION_MESSAGE);
		}
		if (location(mouse, poly6)) {
			HashMap<String, String> sensors = request("reception");
			//JOptionPane jop1 = new JOptionPane();
			JOptionPane.showMessageDialog(null, sensors, "Reception", JOptionPane.INFORMATION_MESSAGE);
		}

	}

	private HashMap<String, String> request(String room) {
		Request req = new Request();
		req.setRequest("select-sensor-poly");
		req.setConditionLabelValue(room);
		String request = SerializationDeserialization.serialization(req);
		String imputFile = null;
		HashMap<String, String> sensors = new HashMap<>();
		try {
			SocketRequestor.getDos().writeUTF(request);
			int rows = SocketRequestor.getDis().readInt();
			System.out.println(rows);
			for (int i = 0; i < rows; i++) {
				imputFile = SocketRequestor.getDis().readUTF();
				Request answer = new Request();
				answer = SerializationDeserialization.deserialization(imputFile);
				Sensor sensor = new Sensor();
				sensor = SerializationDeserialization.deserialization(answer.getObject(), sensor);
				System.out.println(sensor.getFk_type()+sensor.getState());
				sensors.put(sensor.getFk_type(), sensor.getState());
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return sensors;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
