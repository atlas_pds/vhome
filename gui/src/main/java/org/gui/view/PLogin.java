package org.gui.view;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class PLogin extends JPanel {
	private JTextField user;
	private JPasswordField jpass;
	JButton jlogin;
	public PLogin(){
		super();
		this.setLayout(new GridBagLayout());
		JPanel form = new JPanel(new GridLayout(5,1));
		user = new JTextField();
		jpass = new JPasswordField();
		jlogin = new JButton("login");
		form.add(new JLabel("user"));
		form.add(user);
		form.add(new JLabel("password"));
		form.add(jpass);
		form.add(jlogin);
		form.setPreferredSize(new Dimension(400,400));
		this.add(form);
				
	}
	public JTextField getUser() {
		return user;
	}
	public void setUser(JTextField user) {
		this.user = user;
	}
	public JPasswordField getjPass() {
		return jpass;
	}
	public void setPass(JPasswordField pass) {
		this.jpass = pass;
	}
	public JButton getJlogin() {
		return jlogin;
	}
	public void setJlogin(JButton jlogin) {
		this.jlogin = jlogin;
	}
	//


}
