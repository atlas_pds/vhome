package org.gui.view;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PMap extends JPanel {
	
	private JLabel jl ;
	private BufferedImage mapImage;
	private BorderLayout bl;
	 
	public PMap() {
		super();
		Map map = new Map();
		jl = new JLabel("Map");
		bl = new BorderLayout();
		this.setLayout(bl);
		this.add(jl,BorderLayout.NORTH);
		this.add(map,BorderLayout.CENTER);
	}
}
