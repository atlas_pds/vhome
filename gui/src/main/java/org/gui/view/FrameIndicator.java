package org.gui.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.LineBorder;

import org.gui.controller.SocketRequestor;

import entities.Historical;
import entities.Request;
import entities.Room;
import entities.Sensor;
import entities.Sensor_Type;
import serialization.SerializationDeserialization;

import javax.swing.border.BevelBorder;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import java.awt.SystemColor;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.JList;
import javax.swing.JTextPane;

public class FrameIndicator extends JFrame {

	private JPanel contentPane;
	private JPanel txtSensors;
	private JComboBox comboBox_state;
	private JComboBox comboBox_type;
	private JRadioButton chckbxIsInstalled;
	private JPanel txtSensorType;
	private JPanel txtRoom;
	private JRadioButton rdbtnAllDates;
	private JComboBox spinnerTypeOfAlert;
	private JPanel txtAlert2;
	private JTextPane textPaneComparison;
	private int numComparison = 0;
	protected int numberOfLines2 = 0;
	protected int numberOfLines = 0;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameIndicator frame = new FrameIndicator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameIndicator() {
		if(PIndicator.searchIsOpen)
			return;//only one frame
		PIndicator.searchIsOpen = true;
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				PIndicator.searchIsOpen = false;		
			}	
		});
		this.setVisible(true);
		setTitle("Indicators");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1026, 825);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panelSensor = new JPanel();
		panelSensor.setBackground(Color.WHITE);
		tabbedPane.addTab("Sensors", null, panelSensor, null);
		panelSensor.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(329, 13, 652, 629);
		panelSensor.add(scrollPane);
		
		txtSensors = new JPanel();
		scrollPane.setViewportView(txtSensors);
		txtSensors.setBackground(new Color(240, 248, 255));
		txtSensors.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panelButtonSensor = new JPanel();
		panelButtonSensor.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelButtonSensor.setBackground(SystemColor.activeCaption);
		panelButtonSensor.setBounds(12, 13, 285, 712);
		panelSensor.add(panelButtonSensor);
		panelButtonSensor.setLayout(null);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblType.setBounds(32, 45, 65, 39);
		panelButtonSensor.add(lblType);
		
		comboBox_type = new JComboBox();
		comboBox_type.setBackground(Color.WHITE);
		comboBox_type.setModel(new DefaultComboBoxModel(new String[] {"All", "smoke", "temperature", "move", "light"}));
		comboBox_type.setBounds(126, 49, 112, 31);
		panelButtonSensor.add(comboBox_type);
		
		JLabel lblNewLabel = new JLabel("State");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(32, 139, 65, 39);
		panelButtonSensor.add(lblNewLabel);
		
		comboBox_state = new JComboBox();
		comboBox_state.setModel(new DefaultComboBoxModel(new String[] {"All", "on", "off", "alert"}));
		comboBox_state.setBackground(Color.WHITE);
		comboBox_state.setBounds(126, 143, 112, 31);
		panelButtonSensor.add(comboBox_state);
		
		JLabel majLabelSensor = new JLabel(" ");
		majLabelSensor.setBounds(200, 534, 38, 16);
		panelButtonSensor.add(majLabelSensor);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					txtSensors.removeAll();
					Request requestGetSensor = new Request();
					requestGetSensor.setRequest("indicator-sensors");
					requestGetSensor.setObject("");
					String condition;
					if(((String) comboBox_state.getSelectedItem()).contains("All"))
						condition = "";
					else
						condition = "and sensor.state = '" +(String) comboBox_state.getSelectedItem()+"'  ";
					if(!((String) comboBox_type.getSelectedItem()).contains("All"))
						condition = condition +"and sensor_type.type = '" +(String) comboBox_type.getSelectedItem()+"'";
					if(chckbxIsInstalled.isSelected())
						condition = condition+"and sensor.installed = true ";
					requestGetSensor.setConditionLabelValue(condition);
					String request = SerializationDeserialization.serialization(requestGetSensor);
					SocketRequestor.getDos().writeUTF(request);
					//boolean instruction=true;
					int numberOfSensor=0;
					numberOfSensor=SocketRequestor.getDis().readInt();
					JPanel annouceLabel = new JPanel();
					float[] hsb = Color.RGBtoHSB(165, 88, 60, null);
					annouceLabel.setBackground(new Color(86, 191, 235));
					annouceLabel.setForeground(Color.WHITE);
					annouceLabel.setLayout(new GridLayout(1,12));
					annouceLabel.add(new JLabel(" Ip address"));
					annouceLabel.add(new JLabel("Mac addresse"));
					annouceLabel.add(new JLabel("Type"));					
					annouceLabel.add(new JLabel("State"));
					annouceLabel.add(new JLabel("Room"));
					annouceLabel.add(new JLabel("Location "));
					annouceLabel.add(new JLabel("Price"));
					annouceLabel.add(new JLabel("Installation date "));
					annouceLabel.add(new JLabel(" Installed"));
					annouceLabel.add(new JLabel("Scope"));
					annouceLabel.add(new JLabel("Threshold_min"));//TODO
					annouceLabel.add(new JLabel("Threshold_max"));
					txtSensors.add(annouceLabel);
					for (int i = 0; i < numberOfSensor; i++) {
						
					
						String imputFile = SocketRequestor.getDis().readUTF();
						Request getSensor = new Request();
						getSensor=SerializationDeserialization.deserialization(imputFile);
						
						Sensor sens = new Sensor();
						sens = SerializationDeserialization.deserialization(getSensor.getObject(), sens);
						JPanel jpSensor = new JPanel();
						if(i%2 ==0) {
							
							hsb = Color.RGBtoHSB(192, 247, 246, null);
							jpSensor.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
						}
						else {
							hsb = Color.RGBtoHSB(130, 220, 243, null);
							jpSensor.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
						}
						jpSensor.setLayout(new GridLayout(1,12));
						
						jpSensor.add(new JLabel(" "+sens.getIp_address()));
						jpSensor.add(new JLabel(sens.getMac_address().trim()));
						jpSensor.add(new JLabel(sens.getType().trim()));
						jpSensor.add(new JLabel(sens.getState().trim()));
						jpSensor.add(new JLabel(sens.getFk_room().trim()));
						jpSensor.add(new JLabel(sens.getLocation().trim()));
						jpSensor.add(new JLabel(Double.toString(sens.getPrice()).trim()));
						jpSensor.add(new JLabel(sens.getInstallation_date().trim()));
						jpSensor.add(new JLabel(Boolean.toString(sens.isInstalled()).trim()));
						jpSensor.add(new JLabel(Integer.toString(sens.getScope()).trim()));
						jpSensor.add(new JLabel(Integer.toString(sens.getThreshold_min()).trim()));//TODO
						jpSensor.add(new JLabel(Integer.toString(sens.getThreshold_max()).trim()));
						
						
						txtSensors.add(jpSensor);
						
					}
					SocketRequestor.getDis().readUTF();
					//System.out.println("ça boucle avec "+f_name);

				} catch (IOException ed) {
					ed.printStackTrace();
				}
				majLabelSensor.setText("");
			}
		});
		btnSearch.setBounds(59, 638, 162, 39);
		panelButtonSensor.add(btnSearch);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 217, 282, 2);
		panelButtonSensor.add(separator);
		
		chckbxIsInstalled = new JRadioButton("Is installed");
		chckbxIsInstalled.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxIsInstalled.setBackground(SystemColor.activeCaption);
		chckbxIsInstalled.setBounds(19, 268, 219, 46);
		panelButtonSensor.add(chckbxIsInstalled);
		
		
		
		JPanel panelType = new JPanel();
		panelType.setLayout(null);
		panelType.setBackground(Color.WHITE);
		tabbedPane.addTab("Type of sensors", null, panelType, "");
		
		JScrollPane scrollPaneType = new JScrollPane();
		scrollPaneType.setBounds(12, 13, 969, 518);
		panelType.add(scrollPaneType);
		
		txtSensorType = new JPanel();
		scrollPaneType.setViewportView(txtSensorType);
		txtSensorType.setBackground(new Color(240, 248, 255));
		txtSensorType.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panelButtonType = new JPanel();
		panelButtonType.setLayout(null);
		panelButtonType.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelButtonType.setBackground(SystemColor.activeCaption);
		panelButtonType.setBounds(12, 549, 969, 149);
		panelType.add(panelButtonType);
		
		JLabel majLabel = new JLabel(" ");
		majLabel.setBounds(39, 43, 33, 9);
		panelButtonType.add(majLabel);
		
		JButton btnViewTypeProperties = new JButton("View type properties");
		btnViewTypeProperties.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				try {
					txtSensorType.removeAll();
					Request sendedRequest = new Request();
					sendedRequest.setRequest("indicator-typeSensors");
					sendedRequest.setObject("");
					String request = SerializationDeserialization.serialization(sendedRequest);
					SocketRequestor.getDos().writeUTF(request);
					//boolean instruction=true;
					int numberOfLines=0;
					numberOfLines=SocketRequestor.getDis().readInt();
					JPanel annouceLabel = new JPanel();
					float[] hsb = Color.RGBtoHSB(165, 88, 60, null);
					annouceLabel.setBackground(new Color(103, 233, 128));
					annouceLabel.setForeground(Color.WHITE);
					annouceLabel.setLayout(new GridLayout(1,4));
					annouceLabel.add(new JLabel("  Name"));
					annouceLabel.add(new JLabel("Threshold_min"));
					annouceLabel.add(new JLabel("Threshold_max"));					
					annouceLabel.add(new JLabel("Alerts number"));
					txtSensorType.add(annouceLabel);
					for (int i = 0; i < numberOfLines; i++) {
						
					
						String imputFile = SocketRequestor.getDis().readUTF();
						Request getSensor = new Request();
						getSensor=SerializationDeserialization.deserialization(imputFile);
						
						Sensor_Type sens = new Sensor_Type();
						sens = SerializationDeserialization.deserialization(getSensor.getObject(), sens);
						JPanel jpSensor = new JPanel();
						if(i%2 ==1) {
							
							hsb = Color.RGBtoHSB(127, 255, 131, null);
							jpSensor.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
						}
						else {
							hsb = Color.RGBtoHSB(157, 246, 157, null);
							jpSensor.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
						}
						jpSensor.setLayout(new GridLayout(1,4));
						
						jpSensor.add(new JLabel("  "+sens.getType().trim()));
						jpSensor.add(new JLabel(Integer.toString(sens.getSensibility_min()).trim()));//TODO
						jpSensor.add(new JLabel(Integer.toString(sens.getSensibility_max()).trim()));//TODO
						jpSensor.add(new JLabel(Integer.toString(sens.getAlerts_nb()).trim()));
						
						
						txtSensorType.add(jpSensor);
						
					}
					SocketRequestor.getDis().readUTF();

				} catch (IOException ed) {
					ed.printStackTrace();
				}
				majLabel.setText("");
			}
		});
		btnViewTypeProperties.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnViewTypeProperties.setBounds(246, 45, 576, 56);
		panelButtonType.add(btnViewTypeProperties);
		
		
		
		JPanel PanelRoom = new JPanel();
		PanelRoom.setLayout(null);
		PanelRoom.setBackground(Color.WHITE);
		tabbedPane.addTab("Room", null, PanelRoom, null);
		
		JScrollPane scrollPane_Room = new JScrollPane();
		scrollPane_Room.setBounds(329, 13, 652, 629);
		PanelRoom.add(scrollPane_Room);
		
		txtRoom = new JPanel();
		scrollPane_Room.setViewportView(txtRoom);
		txtRoom.setBackground(new Color(240, 248, 255));
		txtRoom.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panelButtonRoom = new JPanel();
		panelButtonRoom.setLayout(null);
		panelButtonRoom.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelButtonRoom.setBackground(SystemColor.activeCaption);
		panelButtonRoom.setBounds(12, 13, 285, 712);
		PanelRoom.add(panelButtonRoom);
		
		JLabel LabelRoomSurfaceValue = new JLabel("");
		LabelRoomSurfaceValue.setFont(new Font("Tahoma", Font.BOLD, 14));
		LabelRoomSurfaceValue.setBounds(30, 126, 223, 55);
		panelButtonRoom.add(LabelRoomSurfaceValue);
		LabelRoomSurfaceValue.setBackground(Color.WHITE);
		LabelRoomSurfaceValue.setOpaque(true);
		
		JLabel lblTotalSurface = new JLabel("Total surface:");
		lblTotalSurface.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblTotalSurface.setBounds(83, 37, 149, 55);
		panelButtonRoom.add(lblTotalSurface);
		
		JButton button = new JButton("Analyse all rooms");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					txtRoom.removeAll();
					Request requestGetSensor = new Request();
					requestGetSensor.setRequest("indicator-room");
					requestGetSensor.setObject("");
					String request = SerializationDeserialization.serialization(requestGetSensor);
					SocketRequestor.getDos().writeUTF(request);
					//boolean instruction=true;
					int numberOfLines=0;
					numberOfLines=SocketRequestor.getDis().readInt();
					JPanel annouceLabel = new JPanel();
					float[] hsb = Color.RGBtoHSB(165, 88, 60, null);
					annouceLabel.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
					annouceLabel.setForeground(Color.WHITE);
					annouceLabel.setLayout(new GridLayout(1,5));
					annouceLabel.add(new JLabel("  Number"));
					annouceLabel.add(new JLabel("Name"));
					annouceLabel.add(new JLabel("Section"));					
					annouceLabel.add(new JLabel("Surface"));
					annouceLabel.add(new JLabel("Number of sensors"));

					txtRoom.add(annouceLabel);
					int surface =0;
					for (int i = 0; i < numberOfLines; i++) {
						
					
						String imputFile = SocketRequestor.getDis().readUTF();
						Request getRequest = new Request();
						getRequest=SerializationDeserialization.deserialization(imputFile);
						
						Room sens = new Room();
						sens = SerializationDeserialization.deserialization(getRequest.getObject(), sens);
						JPanel jpSensor = new JPanel();
						if(i%2 ==0) {
							
							hsb = Color.RGBtoHSB(255, 232, 226, null);
							jpSensor.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
						}
						else {
							hsb = Color.RGBtoHSB(219, 158, 136, null);
							jpSensor.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
						}
						jpSensor.setLayout(new GridLayout(1,5));
						
						jpSensor.add(new JLabel(" "+Integer.toString(sens.getId()).trim()));
						jpSensor.add(new JLabel(sens.getType().trim()));	
						jpSensor.add(new JLabel(sens.getName().trim()));
						jpSensor.add(new JLabel(Integer.toString(sens.getM2()).trim()));
						jpSensor.add(new JLabel(sens.getFloor().trim()));

						surface=surface +sens.getM2();
						txtRoom.add(jpSensor);
					}
					LabelRoomSurfaceValue.setText(surface+" m²");
					SocketRequestor.getDis().readUTF();
					
				} catch (IOException ed) {
					ed.printStackTrace();
				}
			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 16));
		button.setBounds(59, 638, 162, 39);
		panelButtonRoom.add(button);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 217, 282, 2);
		panelButtonRoom.add(separator_1);
		
		JPanel panelAlert = new JPanel();
		panelAlert.setLayout(null);
		panelAlert.setBackground(Color.WHITE);
		tabbedPane.addTab("Alerts", null, panelAlert, null);
		
		JScrollPane scrollPane_Alert = new JScrollPane();
		scrollPane_Alert.setBounds(270, 13, 453, 324);
		panelAlert.add(scrollPane_Alert);
		
		JPanel txtAlert = new JPanel();
		txtAlert.setBackground(new Color(240, 248, 255));
		scrollPane_Alert.setViewportView(txtAlert);
		txtAlert.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panelButtonAlert = new JPanel();
		panelButtonAlert.setLayout(null);
		panelButtonAlert.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelButtonAlert.setBackground(SystemColor.activeCaption);
		panelButtonAlert.setBounds(12, 13, 246, 712);
		panelAlert.add(panelButtonAlert);
		
		JLabel lblEndingDate = new JLabel("Ending date");
		lblEndingDate.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblEndingDate.setBounds(12, 484, 96, 39);
		panelButtonAlert.add(lblEndingDate);
		
		JSpinner comboBoxBeginDate = new JSpinner();
		comboBoxBeginDate.setModel(new SpinnerDateModel(new Date(1557352800000L), null, null, Calendar.DAY_OF_YEAR));
		comboBoxBeginDate.setBackground(Color.WHITE);
		comboBoxBeginDate.setBounds(109, 389, 112, 31);
		panelButtonAlert.add(comboBoxBeginDate);
		
		JLabel lblBeginDate = new JLabel("Begin date");
		lblBeginDate.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblBeginDate.setBounds(12, 385, 96, 39);
		panelButtonAlert.add(lblBeginDate);
		
		JSpinner comboBoxEndingDate = new JSpinner();
		comboBoxEndingDate.setModel(new SpinnerDateModel(new Date(1561068000000L), null, null, Calendar.DAY_OF_YEAR));
		comboBoxEndingDate.setBackground(Color.WHITE);
		comboBoxEndingDate.setBounds(109, 488, 112, 31);
		panelButtonAlert.add(comboBoxEndingDate);
		
		JButton btnShowAlerts = new JButton("Show alerts");
		btnShowAlerts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					txtAlert.removeAll();
					Request requestGetAlert = new Request();
					requestGetAlert.setRequest("indicator-alert");
					requestGetAlert.setObject("");
					String condition="";
					System.out.println((new SimpleDateFormat("yyyy-MM-dd").format(comboBoxEndingDate.getValue())));
					if(!rdbtnAllDates.isSelected()) {
						
						condition = condition+"and historical.date BETWEEN '"+new SimpleDateFormat("yyyy-MM-dd").format(comboBoxBeginDate.getValue())+"' AND '"+new SimpleDateFormat("yyyy-MM-dd").format(comboBoxEndingDate.getValue())+"'";

					}
					if(!((String) spinnerTypeOfAlert.getSelectedItem()).contains("All"))
						condition = condition +"and historical.error_type = '" +(String) spinnerTypeOfAlert.getSelectedItem()+"'";
					
					requestGetAlert.setConditionLabelValue(condition);
					String request = SerializationDeserialization.serialization(requestGetAlert);
					SocketRequestor.getDos().writeUTF(request);
					//boolean instruction=true;
					numberOfLines=0;
					numberOfLines=SocketRequestor.getDis().readInt();
					JPanel annouceLabel = new JPanel();
					float[] hsb = Color.RGBtoHSB(165, 88, 60, null);
					annouceLabel.setBackground(new Color(120, 169, 233));
					annouceLabel.setForeground(Color.WHITE);
					annouceLabel.setLayout(new GridLayout(1,4));
					annouceLabel.add(new JLabel("  Date"));
					annouceLabel.add(new JLabel("Mac address"));
					annouceLabel.add(new JLabel("Type of alert"));					
					annouceLabel.add(new JLabel("Room"));
					numComparison = numberOfLines - numberOfLines2 ;
					textPaneComparison.setText(Integer.toString(numComparison));
					txtAlert.add(annouceLabel);
					for (int i = 0; i < numberOfLines; i++) {
						
					
						String imputFile = SocketRequestor.getDis().readUTF();
						Request getRequest = new Request();
						getRequest=SerializationDeserialization.deserialization(imputFile);
						
						Historical sens = new Historical();
						sens = SerializationDeserialization.deserialization(getRequest.getObject(), sens);
						JPanel jpAlert = new JPanel();
						if(i%2 ==1) {
							
							hsb = Color.RGBtoHSB(120, 177, 233, null);
							jpAlert.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
						}
						else {
							hsb = Color.RGBtoHSB(138, 203, 238, null);
							jpAlert.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
						}
						jpAlert.setLayout(new GridLayout(1,4));
						
						jpAlert.add(new JLabel(" "+sens.getDate().toString().trim()));
						jpAlert.add(new JLabel(sens.getMac_address().trim()));	
						jpAlert.add(new JLabel(sens.getError_type().trim()));
						jpAlert.add(new JLabel(sens.getRoom().trim()));

					
						txtAlert.add(jpAlert);
					}
					
					SocketRequestor.getDis().readUTF();
					
				} catch (IOException ed) {
					ed.printStackTrace();
				}
			}
		});
		btnShowAlerts.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnShowAlerts.setBounds(59, 638, 162, 39);
		panelButtonAlert.add(btnShowAlerts);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(-3, 325, 282, 2);
		panelButtonAlert.add(separator_2);
		
		rdbtnAllDates = new JRadioButton("All dates");
		rdbtnAllDates.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnAllDates.setBackground(SystemColor.activeCaption);
		rdbtnAllDates.setBounds(12, 243, 219, 46);
		panelButtonAlert.add(rdbtnAllDates);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(-6, 199, 285, 2);
		panelButtonAlert.add(separator_3);
		
		JLabel lblTypeOfAlert = new JLabel("Type of error");
		lblTypeOfAlert.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTypeOfAlert.setBounds(12, 80, 96, 39);
		panelButtonAlert.add(lblTypeOfAlert);
		
		spinnerTypeOfAlert = new JComboBox();
		spinnerTypeOfAlert.setModel(new DefaultComboBoxModel(new String[] {"All", "hs", "alert"}));
		spinnerTypeOfAlert.setBackground(Color.WHITE);
		spinnerTypeOfAlert.setBounds(109, 84, 112, 31);
		panelButtonAlert.add(spinnerTypeOfAlert);
		
		JPanel panelButtonAlert2 = new JPanel();
		panelButtonAlert2.setLayout(null);
		panelButtonAlert2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelButtonAlert2.setBackground(new Color(250, 128, 114));
		panelButtonAlert2.setBounds(735, 13, 246, 712);
		panelAlert.add(panelButtonAlert2);
		
		JLabel label = new JLabel("Ending date");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(12, 484, 96, 39);
		panelButtonAlert2.add(label);
		
		JSpinner spinnerBeginDate2 = new JSpinner();
		spinnerBeginDate2.setModel(new SpinnerDateModel(new Date(1557352800000L), null, null, Calendar.DAY_OF_YEAR));
		spinnerBeginDate2.setBackground(Color.WHITE);
		spinnerBeginDate2.setBounds(109, 389, 112, 31);
		panelButtonAlert2.add(spinnerBeginDate2);
		
		JLabel label_1 = new JLabel("Begin date");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_1.setBounds(12, 385, 96, 39);
		panelButtonAlert2.add(label_1);
		
		JSpinner spinner_EndingDate2 = new JSpinner();
		spinner_EndingDate2.setModel(new SpinnerDateModel(new Date(1561154400000L), null, null, Calendar.DAY_OF_YEAR));
		spinner_EndingDate2.setBackground(Color.WHITE);
		spinner_EndingDate2.setBounds(109, 488, 112, 31);
		panelButtonAlert2.add(spinner_EndingDate2);

		
		JSeparator separator_4 = new JSeparator();
		separator_4.setBounds(-3, 325, 282, 2);
		panelButtonAlert2.add(separator_4);
		
		JRadioButton radioButtonAllDates2 = new JRadioButton("All dates");
		radioButtonAllDates2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		radioButtonAllDates2.setBackground(new Color(250, 128, 114));
		radioButtonAllDates2.setBounds(12, 243, 219, 46);
		panelButtonAlert2.add(radioButtonAllDates2);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setBounds(-6, 199, 285, 2);
		panelButtonAlert2.add(separator_5);
		
		JLabel lblTypeOfError = new JLabel("Type of error");
		lblTypeOfError.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTypeOfError.setBounds(12, 80, 96, 39);
		panelButtonAlert2.add(lblTypeOfError);
		
		JComboBox comboBoxTypeOfAlert2 = new JComboBox();
		comboBoxTypeOfAlert2.setModel(new DefaultComboBoxModel(new String[] {"All", "hs", "alert"}));
		comboBoxTypeOfAlert2.setBackground(Color.WHITE);
		comboBoxTypeOfAlert2.setBounds(109, 84, 112, 31);
		panelButtonAlert2.add(comboBoxTypeOfAlert2);
		
		JScrollPane scrollPane_Alert2 = new JScrollPane();
		scrollPane_Alert2.setBounds(270, 401, 453, 324);
		panelAlert.add(scrollPane_Alert2);
		
		txtAlert2 = new JPanel();
		txtAlert2.setBackground(new Color(247, 79, 79));
		scrollPane_Alert2.setViewportView(txtAlert2);
		txtAlert2.setLayout(new GridLayout(0, 1, 0, 0));
		
		
		JButton button_1 = new JButton("Show alerts");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					txtAlert2.removeAll();
					Request requestGetAlert = new Request();
					requestGetAlert.setRequest("indicator-alert");
					requestGetAlert.setObject("");
					String condition="";
					System.out.println((new SimpleDateFormat("yyyy-MM-dd").format(spinner_EndingDate2.getValue())));
					if(!radioButtonAllDates2.isSelected()) {
						
						condition = condition+"and historical.date BETWEEN '"+new SimpleDateFormat("yyyy-MM-dd").format(spinnerBeginDate2.getValue())+"' AND '"+new SimpleDateFormat("yyyy-MM-dd").format(spinner_EndingDate2.getValue())+"'";

					}
					if(!((String) comboBoxTypeOfAlert2.getSelectedItem()).contains("All"))
						condition = condition +"and historical.error_type = '" +(String) comboBoxTypeOfAlert2.getSelectedItem()+"'";
					
					requestGetAlert.setConditionLabelValue(condition);
					String request = SerializationDeserialization.serialization(requestGetAlert);
					SocketRequestor.getDos().writeUTF(request);
					//boolean instruction=true;
					numberOfLines2=0;
					numberOfLines2=SocketRequestor.getDis().readInt();
					JPanel annouceLabel = new JPanel();
					float[] hsb = Color.RGBtoHSB(165, 88, 60, null);
					annouceLabel.setBackground(new Color(220, 106, 106));
					annouceLabel.setForeground(Color.WHITE);
					annouceLabel.setLayout(new GridLayout(1,4));
					annouceLabel.add(new JLabel("  Date"));
					annouceLabel.add(new JLabel("Mac address"));
					annouceLabel.add(new JLabel("Type of alert"));					
					annouceLabel.add(new JLabel("Room"));
					numComparison = numberOfLines - numberOfLines2;
					textPaneComparison.setText(Integer.toString(numComparison));
					txtAlert2.add(annouceLabel);
					for (int i = 0; i < numberOfLines2; i++) {
						
					
						String imputFile = SocketRequestor.getDis().readUTF();
						Request getRequest = new Request();
						getRequest=SerializationDeserialization.deserialization(imputFile);
						
						Historical sens = new Historical();
						sens = SerializationDeserialization.deserialization(getRequest.getObject(), sens);
						JPanel jpAlert = new JPanel();
						if(i%2 ==1) {
							
							hsb = Color.RGBtoHSB(223, 119, 119, null);
							jpAlert.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
						}
						else {
							hsb = Color.RGBtoHSB(228, 139, 139, null);
							jpAlert.setBackground(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
						}
						jpAlert.setLayout(new GridLayout(1,4));
						
						jpAlert.add(new JLabel(" "+sens.getDate().toString().trim()));
						jpAlert.add(new JLabel(sens.getMac_address().trim()));	
						jpAlert.add(new JLabel(sens.getError_type().trim()));
						jpAlert.add(new JLabel(sens.getRoom().trim()));
						
						//lblFrequency2.setText(DecimalFormat.getNumberInstance().format((long)numberOfLines/(daysGap2 / (1000*60*60*24))));//TODO
						txtAlert2.add(jpAlert);
						
					}
					
					SocketRequestor.getDis().readUTF();
					
				} catch (IOException ed) {
					ed.printStackTrace();
				}
			}
		});
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		button_1.setBounds(59, 638, 162, 39);
		panelButtonAlert2.add(button_1);
		
		
		
		JLabel lblComparison = new JLabel("Comparison of the number of alerts:");
		lblComparison.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		lblComparison.setBounds(290, 352, 298, 27);
		panelAlert.add(lblComparison);
		
		textPaneComparison = new JTextPane();
		textPaneComparison.setFont(new Font("Segoe UI", Font.BOLD, 17));
		textPaneComparison.setBackground(new Color(230, 230, 250));
		textPaneComparison.setText(Integer.toString(numComparison));
		textPaneComparison.setBounds(581, 350, 98, 29);
		panelAlert.add(textPaneComparison);
	}
}
