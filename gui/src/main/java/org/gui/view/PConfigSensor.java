package org.gui.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PConfigSensor extends JPanel {
	
	private JButton jbAdd;
	private JTextField sensibility_min =new JTextField();
	private JTextField sensibility_max = new JTextField();
	private JComboBox<String> cbState = new JComboBox<>();
	private JComboBox<String> cbLocalisation = new JComboBox<>();
	private JComboBox<String> cbInstaled = new JComboBox<>();
	private JButton buttonFrameNoConfig = new JButton("No configurated sensors");
	
	private JTextField jtIP = new JTextField();
	private JTextField jtMac = new JTextField();
	private JTextField jtDate = new JTextField();
	public PConfigSensor() {
		super();
		jbAdd = new JButton("Config");
		this.setLayout(new GridLayout(23,1));
		
		//combo type
		cbState.addItem("normal");
		cbState.addItem("broken down");
		cbLocalisation.addItem("North");
		cbLocalisation.addItem("South");
		cbLocalisation.addItem("East");
		cbLocalisation.addItem("West");
		cbInstaled.addItem("true");
		cbInstaled.addItem("false");
		//add labels of columns
		this.add(new JLabel("IP ADDRESS"));
		this.add(jtIP);
		this.add(new JLabel("MAC ADDRESS"));
		this.add(jtMac);
		this.add(new JLabel("STATE"));
		this.add(cbState);
		this.add(new JLabel("threshold_max "));
		this.add(sensibility_max);
		this.add(new JLabel("threshold_min "));
		this.add(sensibility_min);
		this.add(new JLabel("LOCATION"));
		this.add(cbLocalisation);
		this.add(new JLabel("INSTALLATION DATE"));
		this.add(jtDate);
		this.add(new JLabel("intalled"));
		this.add(cbInstaled);
		
		
		this.add(jbAdd);
		this.add(buttonFrameNoConfig);
		buttonFrameNoConfig.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new FrameNoConfig();
				
			}
		});
	}
	public JButton getJbAdd() {
		return jbAdd;
	}
	public JTextField getsensibility_min() {
		return sensibility_min;
	}
	public JComboBox<String> getCbState() {
		return cbState;
	}
	public JComboBox<String> getCbLocalisation() {
		return cbLocalisation;
	}
	public JComboBox<String> getCbInstaled() {
		return cbInstaled;
	}
	public JTextField getJtIP() {
		return jtIP;
	}
	public JTextField getJtMac() {
		return jtMac;
	}
	public JTextField getJtDate() {
		return jtDate;
	}
	public JTextField getSensibility_max() {
		return sensibility_max;
	}
	public void setSensibility_max(JTextField sensibility_max) {
		this.sensibility_max = sensibility_max;
	}
	public JButton getButtonFrameNoConfig() {
		return buttonFrameNoConfig;
	}
	public void setButtonFrameNoConfig(JButton buttonFrameNoConfig) {
		this.buttonFrameNoConfig = buttonFrameNoConfig;
	}
}
