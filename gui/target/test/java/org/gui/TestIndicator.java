package org.gui;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.gui.view.Indicator;

public class TestIndicator {
	public static void main(String[] args) {
		JFrame j = new JFrame("test");
		j.setSize(300, 200);
		Container cPane = j.getContentPane();
		Indicator ind = new Indicator();
		cPane.add(ind);
		j.setVisible(true);
	}

}
