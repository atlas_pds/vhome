package serialization;

import java.sql.ResultSet;


import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import entities.Request;
import entities.Sensor;


public class SerializationDeserialization {
	
	static Gson gson = new Gson();
	
	public static String serialization(Request request) {
			//System.out.println(request.toString());
			String result = gson.toJson(request);
			System.out.println(result);
			System.out.println(request.getClass());
			return  result;
	}
	
	public static Request deserialization(String json) {
		Request result = new Request();
		//System.out.println(result);
		result = gson.fromJson(json, result.getClass());
		//System.out.println(result.getObject().getClass());
		//System.out.println(result.getObject());
		System.out.println(gson.toJson(result.getObject()));
		return result;
	}
	
	public static <T> T deserialization(Object map, T object) {
		if (map.getClass()==LinkedTreeMap.class) {
			T object1 = object;
			object1 = (T) gson.fromJson(gson.toJson(map), object1.getClass());
			return object1;
		}
		return null;
	}
	
	public static void main(String[] args) {
		Request request = new Request();
		request.setRequest("insert-sensor");
		Sensor sensor = new Sensor();
		System.out.println(sensor.getClass());
		sensor.setId(15);
		sensor.setLocation("bathroom");
		request.setObject(sensor);
		String json = serialization(request);
		Request re = deserialization(json);
		Sensor sen = new Sensor();
		sen = deserialization(re.getObject(), sen);
		System.out.println(sen);
		System.out.println(sen.getId());
		System.out.println(sen.getLocation());
		//System.out.println(((LinkedTreeMap) re.getObject()).get("id"));
	}
}
