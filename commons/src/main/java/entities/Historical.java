package entities;

import java.sql.Date;

public class Historical {
    private Date date;
    private String error_type;
    private int fk_sensor;
    private String mac_address;
	private String Room;
	public String getRoom() {
		return Room;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getError_type() {
		return error_type;
	}
	public void setError_type(String error_type) {
		this.error_type = error_type;
	}
	public int getFk_sensor() {
		return fk_sensor;
	}
	public void setFk_sensor(int fk_sensor) {
		this.fk_sensor = fk_sensor;
	}
	public String getMac_address() {
		return mac_address;
	}
	public void setMac_address(String mac_address) {
		this.mac_address = mac_address;
	}
	public void setRoom(String room) {
		this.Room = room;		
	}

}
