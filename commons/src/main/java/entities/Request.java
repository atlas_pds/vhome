package entities;

import java.sql.ResultSet;

public class Request {
	
	private String request;
	
	private Object object;
	private String label;
	private String labelValue;
	private String conditionLabel;
	private String conditionLabelValue;
	
	private ResultSet rs;
	private Boolean success;
	
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	public ResultSet getRs() {
		return rs;
	}
	public void setRs(ResultSet rs) {
		this.rs = rs;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getLabelValue() {
		return labelValue;
	}
	public void setLabelValue(String labelValue) {
		this.labelValue = labelValue;
	}
	public String getConditionLabel() {
		return conditionLabel;
	}
	public void setConditionLabel(String conditionLabel) {
		this.conditionLabel = conditionLabel;
	}
	public String getConditionLabelValue() {
		return conditionLabelValue;
	}
	public void setConditionLabelValue(String conditionLabelValue) {
		this.conditionLabelValue = conditionLabelValue;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}

}
