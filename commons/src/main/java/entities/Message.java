package entities;

import java.sql.Timestamp;

public class Message {
	private Integer sensorId;
	private Float thresholdReached;
	private Timestamp creationDate;
	public Message(Integer sensorId, Float thresholdReached) {
		super();
		this.sensorId = sensorId;
		this.thresholdReached = thresholdReached;
		this.creationDate = new Timestamp(System.currentTimeMillis());
	}
	public Integer getSensorId() {
		return sensorId;
	}
	public Float getThresholdReached() {
		return thresholdReached;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	
	
	
}
