package entities;

import java.sql.Date;

public class Client {
	private String first_name;
	private String last_name; 
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public int getRoom_nb() {
		return room_nb;
	}
	public void setRoom_nb(int room_nb) {
		this.room_nb = room_nb;
	}
	public String getEmergency_nb() {
		return emergency_nb;
	}
	public void setEmergency_nb(String emergency_nb) {
		this.emergency_nb = emergency_nb;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	private Date birthday;
	private String adress;
	private int room_nb ; 
     private String emergency_nb; 
     private String gender;  
      

}
