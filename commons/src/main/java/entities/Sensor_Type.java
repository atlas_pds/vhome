package entities;



public class Sensor_Type {
	private int id;
	private String type;
	private int sensibility_min;
	private int sensibility_max;
	private int alerts_nb;
	public int getId() {
		return id;
	}
	public String getType() {
		return type;
	}
	public int getSensibility_min() {
		return sensibility_min;
	}
	public int getSensibility_max() {
		return sensibility_max;
	}
	public int getAlerts_nb() {
		return alerts_nb;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setSensibility_min(int sensibility_min) {
		this.sensibility_min = sensibility_min;
	}
	public void setSensibility_max(int sensibility_max) {
		this.sensibility_max = sensibility_max;
	}
	public void setAlerts_nb(int alerts_nb) {
		this.alerts_nb = alerts_nb;
	}

}
