package org.commons;

import java.sql.ResultSet;

public class SensorType {
	private String type;
	private String id;
	private String sensibility;
	private String instruction;
	private String type2;
	private String id2;
	private String sensibility2;
	private ResultSet rs;
	private String success;
	
	//constructors
	public SensorType(String type, String id, String sensibility, String instruction) {
		super();
		this.type = type;
		this.id = id;
		this.sensibility = sensibility;
		this.instruction = instruction;
	}
	public SensorType() {
		// TODO nothing yet
	}
	
	public SensorType(String type, String id, String sensibility, String instruction, String type2, String id2,
			String sensibility2, ResultSet rs) {
		super();
		this.type = type;
		this.id = id;
		this.sensibility = sensibility;
		this.instruction = instruction;
		this.type2 = type2;
		this.id2 = id2;
		this.sensibility2 = sensibility2;
		this.rs = rs;
	}
	public SensorType(String s) {
		this.instruction = s;
	}
	
	
	//getter and setter
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSensibility() {
		return sensibility;
	}
	public void setSensibility(String sensibility) {
		this.sensibility = sensibility;
	}
	
	public String getInstruction() {
		return instruction;
	}
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	public String getType2() {
		return type2;
	}
	public void setType2(String type2) {
		this.type2 = type2;
	}
	public String getId2() {
		return id2;
	}
	public void setId2(String id2) {
		this.id2 = id2;
	}
	public String getSensibility2() {
		return sensibility2;
	}
	public void setSensibility2(String sensibility2) {
		this.sensibility2 = sensibility2;
	}
	public ResultSet getRs() {
		return rs;
	}
	public void setRs(ResultSet rs) {
		this.rs = rs;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	
	

}
