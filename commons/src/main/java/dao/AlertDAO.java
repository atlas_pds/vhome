package dao;

import java.sql.Connection;

import entities.Historical;

public interface AlertDAO {
	void insert( Historical alert, Connection cn ) throws DAOException;

}
