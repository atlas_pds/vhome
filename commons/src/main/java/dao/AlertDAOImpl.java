package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import entities.Historical;
import entities.Sensor;


public class AlertDAOImpl implements AlertDAO {

	@Override
	public void insert(Historical alert, Connection cn) throws DAOException {
		int succes = 0;
		
		try {
			Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery("SELECT id FROM Sensor where mac_address = '" + alert.getMac_address() + "'");
			rs.next();
			int fk_sensor = rs.getInt("id");
			rs.close();
			st.close();
			
		String sql = "INSERT INTO Historique (date, error_type,fk_sensor) VALUES (?, ?,?)";
		PreparedStatement preparedstatement = cn.prepareStatement(sql);
		preparedstatement.setObject(1, alert.getDate());
		preparedstatement.setObject(2, alert.getError_type());
		preparedstatement.setObject(3, fk_sensor);
		
		
		
		succes = preparedstatement.executeUpdate();
		
		preparedstatement.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

	

}
