package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import entities.Sensor;

public class SensorDAOImpl implements SensorDAO {

	private static final Connection Connection = null;
	@Override
	public void insert(Sensor sensor, Connection cn) throws DAOException {
		int succes = 0;
		
		try {
			Statement st = cn.createStatement();
			ResultSet rt = st.executeQuery("SELECT id FROM sensor_type where type = '" + sensor.getFk_type() + "'");
			rt.next();
			int fk_type = rt.getInt("id");
			rt.close();
			st.close();
			
		String sql = "INSERT INTO Sensor (mac_address, price,installed, scope, fk_type) VALUES (?, ?,?,?,?)";
		PreparedStatement preparedstatement = cn.prepareStatement(sql);
		preparedstatement.setObject(1, sensor.getMac_address());
		preparedstatement.setObject(2, sensor.getPrice());
		preparedstatement.setObject(3, sensor.isInstalled());
		preparedstatement.setObject(4, sensor.getScope());
		preparedstatement.setObject(5, fk_type);
		
		
		succes = preparedstatement.executeUpdate();
		
		preparedstatement.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ResultSet select(String sql, Connection cn) throws DAOException {
		// TODO Auto-generated method stubi
		return null;
	}

	@Override
	public void delete(Connection cn, String label, String labelValue) throws DAOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Connection cn, String label, String labelValue, String condition, String conditionValue)
			throws DAOException {
		// TODO Auto-generated method stub

	}

	public boolean update(Sensor sensor, Connection cn) {
		try {
		/*Statement st = cn.createStatement();
		ResultSet rt = st.executeQuery("SELECT id FROM room where name = '" + sensor.getFk_room() + "'");
		rt.next();
		int fk_room = rt.getInt("id");*/
		
		String sql = "update Sensor set "
						+ "ip_address = '"+sensor.getIp_address()+"',"
						+ "state = '"+ sensor.getState()+"',"
						+ "location= '"+sensor.getLocation()+"',"
						+ "installation_date= '"+sensor.getInstallation_date()+"',"
						+ "installed = "+sensor.isInstalled()+","
						+ "threshold_min = '"+sensor.getThreshold_min()+"',"
						+ "threshold_max = '"+sensor.getThreshold_max()+"'"
						+ " where mac_address = '"+sensor.getMac_address()+"'"; 
		Statement st = cn.createStatement();
		int bool = st.executeUpdate(sql);
		if(bool!=0)
			return true; 
		
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return false;
		}
	public static void main(String[] args) {
		
		Sensor sensor= new Sensor();
		//Connection cn = DriverManager.getConnection(org.server.connection.pool.Config.getUrl(), org.server.connection.pool.Config.getUser(), org.server.connection.pool.Config.getPassword());
		SensorDAOImpl name = new SensorDAOImpl();
		//name.update( sensor, cn);
	}
}

