package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class UtilityDAO {
	
    private UtilityDAO() {
    }

    /* Silent closure of ResultSet */
    public static void silentClosure( ResultSet resultSet ) {
        if ( resultSet != null ) {
            try {
                resultSet.close();
            } catch ( SQLException e ) {
                System.out.println( "Failed to close resultset : " + e.getMessage() );
            }
        }
    }

    /* Silent closure of Statement */
    public static void silentClosure( Statement statement ) {
        if ( statement != null ) {
            try {
                statement.close();
            } catch ( SQLException e ) {
                System.out.println( "Failed to close Statement : " + e.getMessage() );
            }
        }
    }

    /* Silent closure of connection 
    public static void silentClosure( Connection cn ) {
        if ( cn != null ) {
            DataSource.freeConnection(cn);
        }
    }*/

    /* Fermetures silencieuses du statement et de la connexion 
    public static void fermeturesSilencieuses( Statement statement, Connection connexion ) {
        silentClosure( statement );
        silentClosure( connexion );
    }*/

    /* Silent closure of resultset and statement  */
    public static void silentClosures( ResultSet resultSet, Statement statement ) {
    	silentClosure( resultSet );
    	silentClosure( statement );
        
    }

    /*
     * Initialise la requÃªte prÃ©parÃ©e basÃ©e sur la connexion passÃ©e en argument,
     * avec la requÃªte SQL et les objets donnÃ©s.
     */
    public static PreparedStatement preparedStatementInit( Connection connection, String sql, boolean returnGeneratedKeys, Object... objets ) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement( sql, returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS );
        for ( int i = 0; i < objets.length; i++ ) {
        	System.out.println(i+" : "+objets[i]);
            preparedStatement.setObject( i +1, objets[i] );
        }
        return preparedStatement;
    }
}
